### **Edu Care (Kotlin)**

This application was developed to work as a platfom for students where they will be able to attend MCQ quiz, view lecture sheets and videos etc.

 - **Tech Stack:** ViewModel, LiveData, Coroutine, Work Manager, Firebase Auth (OTP), Firestore, Firebase Function, Katex etc.

![](/assets/edu_care.png)
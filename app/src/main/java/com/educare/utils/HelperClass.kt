package com.educare.utils

import android.graphics.Color
import com.educare.R
import com.google.firebase.Timestamp
import java.text.SimpleDateFormat


object HelperClass {

    fun getClasses(): MutableList<String> {
        return mutableListOf(
            "Class 1" , "Class 2" , "Class 3" , "Class 4" , "Class 5" , "Class 6" , "Class 7",
            "Class 8" , "Class 9-10" , "Class 11" , "Class 12"
        )
    }

    fun timeStampToDateString(timestamp: Timestamp): String {
        val dateFormatter = SimpleDateFormat("EEE, d MMM, yyyy")
        return dateFormatter.format(timestamp.toDate())
    }

    fun getColorStringFromStatus(status: String): Int {
        return when (status) {
            "pending" -> Color.parseColor("#A9A9A9")
            "rejected" -> Color.parseColor("#d32f2f")
            else -> Color.parseColor("#087f23")
        }
    }
    fun getColorResourceFromStatus(status: String): Int {
        return when (status) {
            "pending" -> R.color.pending
            "rejected" -> R.color.rejected
            else -> R.color.approved
        }
    }

    fun getRandomString(length: Int) : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }
}
package com.educare.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.educare.common.models.User
import com.squareup.moshi.Moshi
import java.lang.Exception

object ProfileSharedPrefHelper {

    private const val SHARED_PREF_TAG = "profile-shared-pref-helper"

    private const val USER_TYPE = "user-type"
    private const val USER = "user-any"
    const val USER_ADMIN = "user-admin"
    const val USER_STUDENT = "user-student"

    fun getUserType(context: Context): String? {
        return try {
            getSharedPref(context).getString(USER_TYPE, "")
        } catch (e: Exception) {
            null
        }
    }

    fun saveUserType(context: Context,userType: String) {
        getSharedPref(context).edit().apply {
            putString(USER_TYPE,userType)
        }.apply()
    }

    fun saveUserProfile(context: Context,profile: User) {
        val jsonAdapter = Moshi.Builder().build().adapter(User::class.java)
        val jsonProfile = jsonAdapter.toJson(profile)
        getSharedPref(context).edit().apply {
            putString(USER,jsonProfile)
        }.apply()
    }

    fun deleteSharedPref(context: Context) {
        getSharedPref(context).edit().clear().apply()
    }

    fun getUserProfile(context: Context): User? {
        return try {
            val jsonAdapter = Moshi.Builder().build().adapter(User::class.java)
            val jsonProfile = getSharedPref(context).getString(USER, "")
            jsonAdapter.fromJson(jsonProfile)
        } catch (e: Exception) {
            null
        }
    }

    private fun getSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences(SHARED_PREF_TAG,MODE_PRIVATE)
    }

}
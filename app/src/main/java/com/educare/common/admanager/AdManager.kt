package com.educare.common.admanager

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import timber.log.Timber

class AdManager(var context: Context?, val lifecycle: Lifecycle): LifecycleObserver {

    val interstitialAd by lazy {
        InterstitialAd(context).apply {
            adUnitId = "ca-app-pub-3940256099942544/1033173712"
        }
    }

    init {
        lifecycle.addObserver(this)
        interstitialAd.loadAd(AdRequest.Builder().build())
    }

    fun reload() {
        interstitialAd.loadAd(AdRequest.Builder().build())
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun clearData() {
        Timber.e("Context Cleared")
        context = null
    }

    companion object {

        private var INSTANCE: AdManager? = null

        fun createInstance(context: Context, lifecycle: Lifecycle): AdManager {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: AdManager(context,lifecycle).also { INSTANCE = it }
            }
        }
    }
}
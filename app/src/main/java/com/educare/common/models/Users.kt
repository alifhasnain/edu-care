package com.educare.common.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    val name: String = "",
    val phoneNo: String = "",
    val studentClass: String = ""
)
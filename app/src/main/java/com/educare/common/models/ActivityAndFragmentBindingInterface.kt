package com.educare.common.models

import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.wada811.databinding.dataBinding
import com.wada811.viewbinding.viewBinding

// define and use ViewBindingAppCompatActivity for not forgetting.
open class ViewBindingAppCompatActivity<T : ViewBinding>(@LayoutRes contentLayoutId: Int, bind: (View) -> T) : AppCompatActivity(contentLayoutId) {
    protected val binding by viewBinding(bind)
}

// define and use ViewBindingFragment for not forgetting.
open class ViewBindingFragment<T : ViewBinding>(@LayoutRes contentLayoutId : Int, bind: (View) -> T) : Fragment(contentLayoutId) {
    protected val binding: T by viewBinding(bind)
}

// You can define and use DataBindingAppCompatActivity for not forgetting.
open class DataBindingAppCompatActivity<T : ViewDataBinding>(@LayoutRes contentLayoutId : Int) : AppCompatActivity(contentLayoutId) {
    protected val binding: T by dataBinding()
}

// You can define and use DataBindingFragment for not forgetting.
open class DataBindingFragment<T : ViewDataBinding>(@LayoutRes contentLayoutId : Int) : Fragment(contentLayoutId) {
    protected val binding: T by dataBinding()
}
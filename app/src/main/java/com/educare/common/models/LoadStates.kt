package com.educare.common.models

sealed class LoadStates {
    object Loading : LoadStates()
    class Error(val msg: String = "Something went wrong") : LoadStates()
    class Empty(val msg: String = "No item") : LoadStates()
    object NotEmpty : LoadStates()

    companion object {
        fun <T> decideEmptyOrNot(list: List<T>?,msg: String): LoadStates {
            return if (list.isNullOrEmpty()) { Empty(msg) } else { NotEmpty }
        }
    }
}
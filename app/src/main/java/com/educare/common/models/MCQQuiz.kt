package com.educare.common.models

data class MCQQuiz
(
    val questions: List<MCQQuestion>,
    var correctAns: Int
)
{
    fun checkAns(choice: Int,position: Int): Boolean {
        return questions[position].ans == choice
    }
}
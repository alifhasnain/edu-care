package com.educare.common.models

import com.google.firebase.Timestamp
import com.google.firebase.firestore.Exclude
import com.squareup.moshi.JsonClass

data class Subject(val name: String = "", @Exclude var docID: String = "")

data class Chapter(
    val name: String = "",
    val chapterNo: Float = 0f,
    @Exclude var docID: String = ""
)
@JsonClass(generateAdapter = true)
data class MCQQuestion(
    val question: String = "",
    val choices : MutableList<String> = mutableListOf(),
    val ans: Int = 0,
    @Exclude var docID: String = ""
)

data class MCQQuestionList(
    val questions: MutableList<MCQQuestion> = mutableListOf()
)

data class LectureVideo(
    val title: String = "",
    val url: String = "",
    @Exclude var docID: String = ""
)

data class LectureSheet(
    val title: String = "",
    val url: String = "",
    @Exclude var docID: String = ""
)

@JsonClass(generateAdapter = true)
data class TokenForServer(
    val couponCode: String = "0",
    val usableCount: Int = 0,
    val allowedExamCount: Int = 0,
    val expireDate: Long = 0,
    val dayAvailableAfterActivation: Int = 0
)

data class Token(
    val couponCode: String = "",
    val usableCount: Int = 0,
    val usedCount: Int = 0,
    val allowedExamCount: Int = 0,
    val expireDate: Timestamp = Timestamp(0,0),
    val dayAvailableAfterActivation: Int = 0
)

data class TransactionDetails(
    val contactNo: String = "",
    val requestTime: Timestamp = Timestamp(0,0),
    val status: String = "",
    val trxID: String = "",
    val userContact: String = ""
)

data class PackagesStudent(
    val name: String = "",
    val remainingExams: Int = 0,
    val expiredAt: Timestamp = Timestamp(0,0),
    @Exclude var docID: String = ""
)

data class QuizResults(
    val subject: String = "",
    val totalQuestions: Int = 0,
    val correctAnswer: Int = 0
)
package com.educare.common.loadstatefooter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.educare.R
import com.educare.databinding.FooterRefreshBinding


class FooterLoadStateViewHolder(
    private val binding: FooterRefreshBinding,
    val retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.reload.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        /*if (loadState is LoadState.Error) {
            Toast.makeText(binding.reload.context,"Error while loading data",Toast.LENGTH_SHORT).show()
        }*/
        binding.progressBar.visibility = if(loadState is LoadState.Loading) { View.VISIBLE } else View.GONE
        binding.reload.visibility = if(loadState !is LoadState.Loading) { View.VISIBLE } else View.GONE
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): FooterLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.footer_refresh,parent,false)
            val binding = FooterRefreshBinding.bind(view)
            return FooterLoadStateViewHolder(binding, retry)
        }
    }

}
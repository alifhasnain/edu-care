package com.educare.common.diffcallbacks

import androidx.recyclerview.widget.DiffUtil
import com.educare.common.models.MCQQuestion

class MCQQuestionDiffCallback(private val oldList: List<MCQQuestion>, private val newList: List<MCQQuestion>): DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].question == newList[newItemPosition].question
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}
package com.educare.common.repos

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.educare.common.models.*
import com.educare.features.adminpanel.mcqstudy.recycleradapter.MCQQuestionPagingSource
import com.educare.features.adminpanel.transactions.source.TransactionDetailsPagingSource
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.toObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.tasks.await

class GlobalRepository {

    private val db by lazy { FirebaseFirestore.getInstance() }

    suspend fun fetchSubjects(collectionPath: String): List<Subject> {
        val subjectsSnapshot = db.collection(collectionPath).get().await()
        return subjectsSnapshot.map { queryDocSnapshot ->
            queryDocSnapshot.toObject<Subject>().apply { docID = queryDocSnapshot.id }
        }
    }

    suspend fun fetchChapters(path: String): List<Chapter> {
        val chapterSnapshots = db.collection(path).orderBy("chapterNo").get().await()
        return chapterSnapshots.map { queryDocSnapshot ->
            queryDocSnapshot.toObject<Chapter>().apply { docID = queryDocSnapshot.id }
        }
    }

    fun getMCQQuestionStream(path: String): Flow<PagingData<MCQQuestion>> {
        return Pager(
            config = PagingConfig(pageSize = 8),
            pagingSourceFactory = { MCQQuestionPagingSource(path) }
        ).flow
    }

    suspend fun fetchMCQQuestions(path: String): List<MCQQuestion> {
        return db.collection(path).get().await().toObjects(MCQQuestion::class.java).shuffled()
    }

    suspend fun fetchMCQQuestions(path: String,limit: Long): QuerySnapshot {
        return db.collection(path).limit(limit).get().await()
    }

    suspend fun fetchMCQQuestionsAfter(path: String, limit: Long, after: DocumentSnapshot): QuerySnapshot {
        return db.collection(path).startAfter(after).limit(limit).get().await()
    }

    suspend fun deleteSingleMCQQuestion(path: String,docID: String) {
        db.document("$path/$docID").delete().await()
    }

    suspend fun updateSingleMCQQuestion(doc: String,question: MCQQuestion) {
        db.document(doc).set(question).await()
    }

    suspend fun addLectureVideo(path: String, data: LectureVideo) {
        db.collection(path).add(data).await()
    }

    suspend fun fetchLectureVideos(path: String): List<LectureVideo> {
        return db.collection(path).get().await().map { it.toObject<LectureVideo>() }
    }

    suspend fun deleteLectureVideo(path: String,docID: String) {
        db.collection(path).document(docID).delete().await()
    }

    suspend fun saveEditedLectureVideo(path: String, lectureVideo: LectureVideo) {
        db.collection(path).document(lectureVideo.docID).set(lectureVideo).await()
    }

    suspend fun fetchLectureSheets(path: String): List<LectureSheet> {
        return db.collection(path).get().await().map { it.toObject(LectureSheet::class.java)}
    }

    suspend fun addLectureSheet(path: String, lectureSheet: LectureSheet) {
        db.collection(path).add(lectureSheet).await()
    }

    suspend fun updateLectureSheet(path: String, lectureSheet: LectureSheet) {
        db.document("$path/${lectureSheet.docID}").set(lectureSheet).await()
    }

    suspend fun deleteLectureSheet(path: String, docID: String) {
        db.collection(path).document(docID).delete().await()
    }

    fun fetchTransactionDetailsStream(path: String): Flow<PagingData<TransactionDetails>> {
        return Pager(
            config = PagingConfig(pageSize = 10),
            pagingSourceFactory = { TransactionDetailsPagingSource(path) }
        ).flow
    }

    suspend fun fetchTransactionDetails(path: String,loadSize: Int): QuerySnapshot {
        return db.collection(path).orderBy("requestTime").limit(loadSize.toLong()).get().await()
    }

    suspend fun fetTransactionDetailsAfter(path: String, loadSize: Int,after: DocumentSnapshot): QuerySnapshot {
        return db.collection(path).orderBy("requestTime").limit(loadSize.toLong()).startAfter(after).get().await()
    }

    suspend fun fetchCurrentUserProfile(): User {
        val phoneNo = FirebaseAuth.getInstance().currentUser?.phoneNumber
        val profileSnapshot = FirebaseFirestore.getInstance().document("users/$phoneNo").get().await()
        return profileSnapshot.toObject<User>()!!
    }

}
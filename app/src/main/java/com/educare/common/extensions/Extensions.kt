package com.educare.common.extensions


import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.firebase.firestore.QuerySnapshot
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import timber.log.Timber

// Any
inline fun runWithoutException(block: () -> Unit) {
    try {
        block()
    } catch (e: Exception) {
        Timber.e(e)
    }
}


/*
* [Activity] Extension Function
*/
fun AppCompatActivity.makeToast(text: String?) {
    text?.let { Toast.makeText(this,text,Toast.LENGTH_SHORT).show() }
}

fun AppCompatActivity.setActionBarTitle(title: String) {
    this.supportActionBar?.title = title
}


/*
* [Context] Extension Function
*/
inline fun <reified T> Context.startActivity(bundle: Bundle? = null) {
    startActivity(Intent(this,T::class.java).apply { bundle?.let { putExtras(it) } })
}

inline fun <reified T> Context.clearStackAndStartActivity() {
    startActivity(Intent(this,T::class.java).apply {
        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    })
}


/*
* [Fragment] Extension Function
*/
fun Fragment.makeToast(text: String?) {
    this.context?.let { context ->
        text?.let { Toast.makeText(context,text,Toast.LENGTH_SHORT).show() }
    }
}

// Int
val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

// Moshi
inline fun <reified T> List<T>.toJSON(): String {
    val type = Types.newParameterizedType(List::class.java, T::class.java)
    val adapter: JsonAdapter<List<T>> = Moshi.Builder().build().adapter(type)
    return adapter.toJson(this)
}

inline fun <reified T> String.fromJson(): List<T> {
    val type = Types.newParameterizedType(List::class.java, T::class.java)
    val adapter: JsonAdapter<List<T>> = Moshi.Builder().build().adapter(type)
    return adapter.fromJson(this) ?: listOf()
}

/*
* [MutableLiveData] Extension Function
*/

fun <T> MutableLiveData<T>.setValueThenNullify(value: T) {
    this.value = value
    this.value = null
}

/*
* [QuerySnapshot] Extensions
*/

inline fun <reified T> QuerySnapshot.toList(): MutableList<T> {
    val resultList = mutableListOf<T>()
    for(doc in this) {
        resultList.add(doc.toObject(T::class.java))
    }
    return resultList
}

/*
* [EditText] Extenstions
*/

val EditText.string: String
        get() = this.text.toString()

val EditText.trimmedString: String
        get() = this.text.toString().trim()

/*
* [ViewPager2] Extension Function
*/

fun ViewPager2.reduceDragSensitivity(reduceSensitivity: Int = 3) {
    val recyclerViewField = ViewPager2::class.java.getDeclaredField("mRecyclerView")
    recyclerViewField.isAccessible = true
    val recyclerView = recyclerViewField.get(this) as RecyclerView

    val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop")
    touchSlopField.isAccessible = true
    val touchSlop = touchSlopField.get(recyclerView) as Int
    touchSlopField.set(recyclerView, touchSlop*reduceSensitivity)  // "8" was obtained experimentally
}

/*
* Visibility
*/

fun toVisibility(constraint: Boolean): Int = if (constraint) {
    View.VISIBLE
} else {
    View.GONE
}

/*
* [View]
* */

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}
package com.educare.common.adapter

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.educare.common.models.LoadStates

@BindingAdapter("bindLoadingState")
fun uiStateWhileLoading(view: View, state: LoadStates?) {
    state?.let {
        view.visibility = if (state is LoadStates.Loading) { View.VISIBLE } else { View.GONE }
    }
}

@BindingAdapter("bindErrorState")
fun uiStateWhenError(view: LinearLayout, state: LoadStates?) {
    state?.let {
        view.visibility = if(state is LoadStates.Error) {
            val msg = view.getChildAt(1) as TextView
            msg.text = state.msg
            View.VISIBLE
        } else { View.GONE }
    }
}

@BindingAdapter("bindEmptyState")
fun uiStateWhenEmpty(view: LinearLayout, state: LoadStates?) {
    state?.let {
        if(state is LoadStates.Empty) {
            val msg = view.getChildAt(1) as TextView
            msg.text = state.msg
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }
}

@BindingAdapter("bindNonEmptyState")
fun uiStateWhenNotEmpty(view: View, state: LoadStates?) {
    state?.let {
        view.visibility = if (state is LoadStates.NotEmpty) { View.VISIBLE } else { View.GONE }
    }
}

@BindingAdapter("bindDisableState")
fun bindButtonDisableState(view: View, state: LoadStates) {
    view.isEnabled = state !is LoadStates.Loading
}

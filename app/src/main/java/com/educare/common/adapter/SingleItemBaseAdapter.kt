package com.educare.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView

abstract class SingleItemBaseAdapter<T>: RecyclerView.Adapter<SingleItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, layoutID: Int): SingleItemViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            layoutID,
            parent,
            false
        )
        return SingleItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SingleItemViewHolder, position: Int) {
        val data: T = getItemForPosition(position)
        holder.bind(data)
        holder.bindAdapter(this)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    abstract fun onLongClicked(item: T): Boolean

    abstract fun clickedItem(item: T)

    protected abstract fun getItemForPosition(position: Int): T

    protected abstract fun getLayoutIdForPosition(position: Int): Int

}

class SingleItemViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun <T> bind(data: T) {
        binding.setVariable(BR.item, data)
    }

    fun <T> bindAdapter(adapter: SingleItemBaseAdapter<T>) {
        binding.setVariable(BR.adapter, adapter)
        binding.executePendingBindings()
    }
}
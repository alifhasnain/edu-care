package com.educare.features.youtubeplayer

import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.educare.R
import com.educare.databinding.ActivityYouTubePlayerBinding
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.wada811.viewbinding.viewBinding

class YouTubePlayerActivity : AppCompatActivity() {

    private var url: String? = null

    private val binding by viewBinding { ActivityYouTubePlayerBinding.bind(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        url = intent.getStringExtra("url")
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_you_tube_player)

        lifecycle.addObserver(binding.youtubePlayerView)
        binding.youtubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                youTubePlayer.loadVideo(url!!,0f)
            }
        })
        binding.youtubePlayerView.enterFullScreen()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("url",url)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        url = savedInstanceState.getString("url")
        super.onRestoreInstanceState(savedInstanceState)
    }
}
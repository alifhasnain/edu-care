package com.educare.features.refer

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.databinding.FragmentReferSomeoneBinding
import com.google.firebase.functions.FirebaseFunctions
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber


class ReferSomeone : Fragment(R.layout.fragment_refer_someone) {

    private val binding by viewBinding { FragmentReferSomeoneBinding.bind(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        fetchTokenFromServer()
    }

    private fun fetchTokenFromServer() {
        runWithoutException {
            FirebaseFunctions.getInstance("asia-east2")
                .getHttpsCallable("getMyCoupon")
                .call()
                .addOnSuccessListener {
                    binding.token.text = it.data.toString()
                }
                .addOnFailureListener {
                    Timber.e(it)
                    makeToast(it.message)
                }
        }
    }

    private fun initializeProperties() {
        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launch {
                fetchTokenFromServer()
                delay(1500)
                binding.swipeRefresh.isRefreshing = false
            }
        }

        binding.copy.setOnClickListener {
            val clipboardmanager =  requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipdata = ClipData.newPlainText("token",binding.token.text)
            clipboardmanager.setPrimaryClip(clipdata)
            makeToast("copied")
        }
    }

}
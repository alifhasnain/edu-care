package com.educare.features.signin

import `in`.aabhasjindal.otptextview.OTPListener
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.educare.R
import com.educare.common.extensions.clearStackAndStartActivity
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.startActivity
import com.educare.common.models.DataBindingAppCompatActivity
import com.educare.common.models.User
import com.educare.databinding.ActivityVerifyOTPBinding
import com.educare.features.completeprofile.CompleteProfile
import com.educare.features.home.MainActivity
import com.educare.utils.ProfileSharedPrefHelper
import com.educare.utils.ProfileSharedPrefHelper.USER_ADMIN
import com.educare.utils.ProfileSharedPrefHelper.USER_STUDENT
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import timber.log.Timber

class VerifyOTP : DataBindingAppCompatActivity<ActivityVerifyOTPBinding>(R.layout.activity_verify_o_t_p) {

    private val auth by lazy { FirebaseAuth.getInstance() }

    private val token by lazy { intent.extras?.getString("code") ?: "" }

    private val db by lazy { Firebase.firestore }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.contactNo.text = intent.extras?.getString("phoneNo")
        initializeListeners()
    }

    fun signInUser(otp: String) {

        hideKeyboard()
        binding.progressBarHolder.visibility = View.VISIBLE

        auth.signInWithCredential(PhoneAuthProvider.getCredential(token,otp)).addOnSuccessListener {
            checkProfileCompletion()
        }.addOnFailureListener {
            makeToast(it.message)
            binding.progressBarHolder.visibility = View.GONE
        }
    }

    private fun checkProfileCompletion() {

        binding.progressBarHolder.visibility = View.VISIBLE

        val profileRef = db.collection("users").document(auth.currentUser?.phoneNumber!!)
        profileRef.get().addOnSuccessListener { profileSnapshot ->
            if(profileSnapshot.exists()) {
                val profile = profileSnapshot.toObject<User>()
                ProfileSharedPrefHelper.saveUserProfile(this,profile!!)
                checkUserTypeAndSignIn()
            } else {
                startActivity<CompleteProfile>()
            }
            binding.progressBarHolder.visibility = View.GONE
        }.addOnFailureListener {
            Timber.e(it)
            binding.progressBarHolder.visibility = View.GONE
            makeToast(it.message)
        }
    }

    private fun checkUserTypeAndSignIn() {
        binding.progressBarHolder.visibility = View.VISIBLE
        if(auth.currentUser != null) {
            auth.currentUser?.getIdToken(false)?.addOnSuccessListener { result ->
                if(result.claims["admin"] !=null) {
                    ProfileSharedPrefHelper.saveUserType(this,USER_ADMIN)
                } else {
                    ProfileSharedPrefHelper.saveUserType(this,USER_STUDENT)
                }
                clearStackAndStartActivity<MainActivity>()
                binding.progressBarHolder.visibility = View.GONE
            }?.addOnFailureListener {
                Timber.e(it)
                binding.progressBarHolder.visibility = View.GONE
                makeToast(it.message)
            }
        }
    }

    private fun initializeListeners() {

        binding.otpView.requestFocusOTP()
        binding.otpView.otpListener = object : OTPListener {

            override fun onInteractionListener() {

            }

            override fun onOTPComplete(otp: String) {
                signInUser(otp)
            }

        }
    }

    private fun hideKeyboard() {
        val view: View? = this.currentFocus
        if (view != null) {
            val imm: InputMethodManager? = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}
package com.educare.features.signin

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import com.educare.R
import com.educare.common.extensions.clearStackAndStartActivity
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.startActivity
import com.educare.common.models.DataBindingAppCompatActivity
import com.educare.common.models.User
import com.educare.databinding.ActivitySignInBinding
import com.educare.features.completeprofile.CompleteProfile
import com.educare.features.home.MainActivity
import com.educare.utils.ProfileSharedPrefHelper
import com.educare.utils.ProfileSharedPrefHelper.USER_ADMIN
import com.educare.utils.ProfileSharedPrefHelper.USER_STUDENT
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SignIn : DataBindingAppCompatActivity<ActivitySignInBinding>(R.layout.activity_sign_in) {

    private val auth by lazy { FirebaseAuth.getInstance() }

    private val phoneAuth by lazy { PhoneAuthProvider.getInstance() }

    private val db by lazy { Firebase.firestore }

    private val authListener by lazy {
        FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser != null) {
                showProgress(true)
                firebaseAuth.currentUser?.getIdToken(false)
                    ?.addOnSuccessListener {
                        if(ProfileSharedPrefHelper.getUserProfile(this) == null) {
                            return@addOnSuccessListener
                        }
                        if(it.claims["admin"] != null) {
                            ProfileSharedPrefHelper.saveUserType(this,USER_ADMIN)
                        } else {
                            ProfileSharedPrefHelper.saveUserType(this,USER_STUDENT)
                        }
                        clearStackAndStartActivity<MainActivity>()
                    }
                    ?.addOnFailureListener {
                        Timber.e(it)
                        checkForOfflineInfo()
                    }
                showProgress(false)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.presenter = this
    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(authListener)
    }

    override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(authListener)
    }

    override fun onBackPressed() {
        if(binding.progressBarHolder.isVisible) {
            binding.progressBarHolder.visibility = View.GONE
        } else {
            super.onBackPressed()
        }
    }

    fun sendOTPToUser(phoneNo: String) {
        auth.removeAuthStateListener(authListener)
        showProgress(true)

        val updatedPhoneNo = if(!phoneNo.startsWith("+88")) { "+88$phoneNo" } else phoneNo

        phoneAuth.verifyPhoneNumber(
            updatedPhoneNo,
            60,
            TimeUnit.SECONDS,
            this,
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                    signInWithCredential(p0)
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    makeToast(e.message)
                    showProgress(false)
                }

                override fun onCodeSent(token: String, p1: PhoneAuthProvider.ForceResendingToken) {
                    showProgress(false)
                    startActivity<VerifyOTP>(bundleOf("code" to token,"phoneNo" to updatedPhoneNo))
                }

            }
        )
    }

    private fun signInWithCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential).addOnSuccessListener {
            checkProfileCompletion()
        }.addOnFailureListener {
            showProgress(false)
            makeToast(it.message)
            binding.progressBarHolder.visibility = View.GONE
        }
    }

    private fun checkProfileCompletion() {

        binding.progressBarHolder.visibility = View.VISIBLE

        val profileRef = db.collection("users").document(auth.currentUser?.phoneNumber!!)
        profileRef.get().addOnSuccessListener { profileSnapshot ->
            if(profileSnapshot.exists()) {
                val profile = profileSnapshot.toObject<User>()
                ProfileSharedPrefHelper.saveUserProfile(this,profile!!)
                checkUserTypeAndSignIn()
            } else {
                startActivity<CompleteProfile>()
            }
            binding.progressBarHolder.visibility = View.GONE
        }.addOnFailureListener {
            Timber.e(it)
            binding.progressBarHolder.visibility = View.GONE
            makeToast(it.message)
        }
    }

    private fun checkUserTypeAndSignIn() {

        showProgress(true)

        if(auth.currentUser != null) {
            auth.currentUser?.getIdToken(false)?.addOnSuccessListener { result ->
                if(result.claims["admin"] !=null) {
                    ProfileSharedPrefHelper.saveUserType(this,USER_ADMIN)
                } else {
                    ProfileSharedPrefHelper.saveUserType(this,USER_STUDENT)
                }
                clearStackAndStartActivity<MainActivity>()
                showProgress(false)
            }?.addOnFailureListener {
                Timber.e(it)
                showProgress(false)
                makeToast(it.message)
            }
        }
    }

    private fun checkForOfflineInfo() {
        if (ProfileSharedPrefHelper.getUserProfile(this) == null || ProfileSharedPrefHelper.getUserType(this) == null) {
            signOut()
            makeToast("Please login again")
        } else {
            clearStackAndStartActivity<MainActivity>()
        }
    }

    private fun signOut() {
        try { auth.signOut() } catch (e: Exception) { Timber.e(e) }
    }

    private fun showProgress(loading: Boolean) {
        if(loading) {
            binding.progressBarHolder.visibility = View.VISIBLE
        } else {
            binding.progressBarHolder.visibility = View.GONE
        }
    }

}
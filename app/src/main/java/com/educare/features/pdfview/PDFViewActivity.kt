package com.educare.features.pdfview

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.educare.R
import com.educare.databinding.ActivityPDFViewBinding
import com.wada811.viewbinding.viewBinding

class PDFViewActivity : AppCompatActivity() {

    private val binding by viewBinding { ActivityPDFViewBinding.bind(it) }

    private var pdfURL: String? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_p_d_f_view)

        initializeProperties()

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(pdfURL ?: "")
                return true
            }
        }
        binding.webView.loadUrl(pdfURL ?: "")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("url",pdfURL)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        pdfURL = savedInstanceState.getString("url")
        super.onRestoreInstanceState(savedInstanceState)
    }

    private fun initializeProperties() {
        pdfURL = intent.getStringExtra("url")
    }

}
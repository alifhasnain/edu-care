package com.educare.features.mcqquiz

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.educare.R
import com.educare.common.extensions.fromJson
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.extensions.toJSON
import com.educare.common.models.MCQQuestion
import com.educare.databinding.FragmentMCQQuizStudentBinding
import com.google.firebase.functions.FirebaseFunctions
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber


class MCQQuizStudent : Fragment(R.layout.fragment_m_c_q_quiz_student) {

    private var chapters: List<String>? = null

    private var subject: String? = null

    private var collectionPath: String? = null

    //private var packageID: String? = null

    private var questions: List<MCQQuestion>? = null

    private var examStarted: Boolean = false

    private var timerJob: Job? = null

    private var currentQuestion: Int = 0

    private val selections = mutableListOf<Int>()

    private val binding by viewBinding { FragmentMCQQuizStudentBinding.bind(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("chapters", chapters!!.toJSON())
        outState.putString("path", collectionPath)
        outState.putString("subject",subject)
        //outState.putString("package", packageID)
        super.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        timerJob?.cancel()
        super.onDestroyView()
    }

    private fun beginExam() {
        binding.warningHolder.visibility = View.GONE
        binding.quizHolder.visibility = View.VISIBLE
        binding.next.visibility = View.VISIBLE
        examStarted = true
        startTimer()
        nextQuestion()
    }

    private fun startTimer() {
        timerJob?.cancel()
        timerJob = lifecycleScope.launch {
            repeat(31) {
                runWithoutException {
                    delay(1000)
                    ensureActive()
                    binding.timer.text = (30-it).toString()
                }
            }
            if(currentQuestion == questions!!.size) {
                ensureActive()
                addAnswer()
                finish()
            } else {
                ensureActive()
                nextQuestion()
            }
        }
    }

    private fun addAnswer() {
        when {
            binding.checkbox1.isChecked -> selections.add(0)
            binding.checkbox2.isChecked -> selections.add(1)
            binding.checkbox3.isChecked -> selections.add(2)
            binding.checkbox4.isChecked -> selections.add(3)
            else -> selections.add(5)
        }
    }

    private fun nextQuestion() {
        startTimer()
        addAnswer()
        when (currentQuestion) {
            questions!!.size-1 -> {
                binding.next.text = "Finish"
                setDataToView(questions!![currentQuestion])
                currentQuestion++
            }
            questions!!.size -> {
                finish()
            }
            else -> {
                setDataToView(questions!![currentQuestion])
                currentQuestion++
            }
        }
    }

    private fun setDataToView(item: MCQQuestion) {
        binding.checkbox1.isChecked = false
        binding.checkbox2.isChecked = false
        binding.checkbox3.isChecked = false
        binding.checkbox4.isChecked = false

        binding.question.setDisplayText(item.question)
        binding.choiceOne.setDisplayText(item.choices[0])
        binding.choiceTwo.setDisplayText(item.choices[1])
        binding.choiceThree.setDisplayText(item.choices[2])
        binding.choiceFour.setDisplayText(item.choices[3])
    }

    private fun finish() {
        timerJob?.cancel()
        selections.removeAt(0)
        findNavController().navigate(
            R.id.action_MCQQuizStudent_to_quizResultFragment,
            bundleOf(
                "questions" to questions!!.toJSON(),
                "answers" to selections.toJSON(),
                "subject" to subject
            )
        )
    }

    private fun startExam() {
        AlertDialog.Builder(requireContext()).apply { 
            setTitle("Are you sure?")
            setMessage("Have you read everything that was said and want to proceed?")
            setPositiveButton("yes") { _,_ ->
                fetchExamQuestionFromServer()
                makeToast("Fetching questions from server")
            }
            setNegativeButton("cancel",null)
        }.create().show()
    }

    private fun fetchExamQuestionFromServer() {
        val jsonObject = JSONObject()
        jsonObject.put("collectionPath",collectionPath)
        jsonObject.put("chapters",JSONArray(chapters))
        //jsonObject.put("packageID",packageID)

        FirebaseFunctions.getInstance("asia-east2")
            .getHttpsCallable("fetchMCQExamQuestions2")
            .call(jsonObject.toString())
            .addOnSuccessListener {
                questions = it.data.toString().fromJson()
                beginExam()
            }
            .addOnFailureListener {
                Timber.e(it)
                makeToast("Failed to load data from server")
            }
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        chapters = arguments?.getString("chapters")!!.fromJson()
        subject = arguments?.getString("subject")
        //packageID = arguments?.getString("package")

        binding.startExam.setOnClickListener { startExam() }

        binding.next.setOnClickListener {
            nextQuestion()
            lifecycleScope.launch {
                it.isEnabled = false
                delay(800)
                ensureActive()
                it.isEnabled = true
            }
        }
        binding.checkbox1.setOnCheckedChangeListener { _, state ->
            if (state) {
                binding.checkbox2.isChecked = false
                binding.checkbox3.isChecked = false
                binding.checkbox4.isChecked = false
            }
        }
        binding.checkbox2.setOnCheckedChangeListener { _, state ->
            if (state) {
                binding.checkbox1.isChecked = false
                binding.checkbox3.isChecked = false
                binding.checkbox4.isChecked = false
            }
        }
        binding.checkbox3.setOnCheckedChangeListener { _, state ->
            if (state) {
                binding.checkbox1.isChecked = false
                binding.checkbox2.isChecked = false
                binding.checkbox4.isChecked = false
            }
        }
        binding.checkbox4.setOnCheckedChangeListener { _, state ->
            if (state) {
                binding.checkbox1.isChecked = false
                binding.checkbox2.isChecked = false
                binding.checkbox3.isChecked = false
            }
        }
    }
}
package com.educare.features.mcqquiz.quizresults

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.educare.R
import com.educare.common.models.MCQQuestion
import com.educare.databinding.ListItemQuizEvaluationBinding

class QuizResultRecyclerViewAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val questions = mutableListOf<MCQQuestion>()

    private val answers = mutableListOf<Int>()

    fun updateItems(updatedQuestions: List<MCQQuestion>, updatedAnswers: List<Int>) {
        questions.clear()
        questions.addAll(updatedQuestions)
        answers.clear()
        answers.addAll(updatedAnswers)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListItemQuizEvaluationBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return questions.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(questions[holder.absoluteAdapterPosition],answers[holder.absoluteAdapterPosition])
    }

    class ViewHolder(private val binding: ListItemQuizEvaluationBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(mcqQuestion: MCQQuestion,answer: Int) {
            clearExistingSelections()
            binding.question.setDisplayText(mcqQuestion.question)
            binding.choiceOne.setDisplayText(mcqQuestion.choices[0])
            binding.choiceTwo.setDisplayText(mcqQuestion.choices[1])
            binding.choiceThree.setDisplayText(mcqQuestion.choices[2])
            binding.choiceFour.setDisplayText(mcqQuestion.choices[3])

            when(answer) {
                0 -> {
                    binding.checkbox1.isChecked = true
                    binding.holder1.setBackgroundResource(R.color.red_light)
                }
                1 -> {
                    binding.checkbox2.isChecked = true
                    binding.holder2.setBackgroundResource(R.color.red_light)
                }
                2 -> {
                    binding.checkbox3.isChecked = true
                    binding.holder3.setBackgroundResource(R.color.red_light)
                }
                3 -> {
                    binding.checkbox4.isChecked = true
                    binding.holder4.setBackgroundResource(R.color.red_light)
                }
            }
            when(mcqQuestion.ans) {
                0 -> {
                    binding.checkbox1.isChecked = true
                    binding.holder1.setBackgroundResource(R.color.green_light)
                }
                1 -> {
                    binding.checkbox2.isChecked = true
                    binding.holder2.setBackgroundResource(R.color.green_light)
                }
                2 -> {
                    binding.checkbox3.isChecked = true
                    binding.holder3.setBackgroundResource(R.color.green_light)
                }
                3 -> {
                    binding.checkbox4.isChecked = true
                    binding.holder4.setBackgroundResource(R.color.green_light)
                }
            }
        }

        private fun clearExistingSelections() {
            binding.holder1.background = null
            binding.holder2.background = null
            binding.holder3.background = null
            binding.holder4.background = null

            binding.checkbox1.isChecked = false
            binding.checkbox2.isChecked = false
            binding.checkbox3.isChecked = false
            binding.checkbox4.isChecked = false
        }
    }

}
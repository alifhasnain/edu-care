package com.educare.features.mcqquiz.selectchapter

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.toJSON
import com.educare.common.models.Chapter
import com.educare.databinding.FragmentChaptersForMCQQuizBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import timber.log.Timber


class ChaptersForMCQQuiz : Fragment(R.layout.fragment_chapters_for_m_c_q_quiz) {

    private var collectionPath: String? = null

    private var subject: String? = null

    private var checkedChapters = mutableListOf<String>()

    private val binding by viewBinding { FragmentChaptersForMCQQuizBinding.bind(it) }

    private val adapter = MCQQuizChaptersRecyclerAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchChapters()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("subject",subject)
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchChapters() {
        FirebaseFirestore.getInstance().collection("$collectionPath/chapters")
            .addSnapshotListener { querySnapshot, e ->
                if (e == null) {
                    checkedChapters.clear()
                    adapter.updateItems(querySnapshot!!.map { it.toObject<Chapter>().apply { docID = it.id } })
                } else {
                    Timber.e(e)
                    makeToast("Error occurred while loading data")
                }
            }
    }

    private fun checkSelectionAndProceed() {
        if (checkedChapters.size == 0) {
            makeToast("No chapters selected")
            return
        }
        val bundle = bundleOf(
            "path" to "$collectionPath/chapters",
            "chapters" to checkedChapters.toJSON(),
            "subject" to subject
        )
        findNavController().navigate(R.id.action_chaptersForMCQQuiz_to_MCQQuizStudent, bundle)
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onCheckChanged = { item,state ->
            if (state) {
                checkedChapters.add(item.docID)
            } else {
                checkedChapters = checkedChapters.filter { it != item.docID } as MutableList
            }
        }
    }

    private fun initializeProperties() {
        checkedChapters.clear()
        collectionPath = arguments?.getString("path")
        subject = arguments?.getString("subject")
        binding.proceed.setOnClickListener { checkSelectionAndProceed() }
    }
}
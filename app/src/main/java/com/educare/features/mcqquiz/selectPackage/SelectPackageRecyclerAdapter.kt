package com.educare.features.mcqquiz.selectPackage

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.PackagesStudent

class SelectPackageRecyclerAdapter: SingleItemBaseAdapter<PackagesStudent>() {

    private val items = mutableListOf<PackagesStudent>()

    var onItemClicked: ((PackagesStudent) -> Unit)? = null

    fun updateItems(updatedItems: List<PackagesStudent>) {
        items.clear()
        items.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: PackagesStudent): Boolean {
        return true
    }

    override fun clickedItem(item: PackagesStudent) {
        onItemClicked?.invoke(item)
    }

    override fun getItemForPosition(position: Int): PackagesStudent {
        return items[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_select_package
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
package com.educare.features.mcqquiz.quizresults

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.fromJson
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.toJSON
import com.educare.common.models.MCQQuestion
import com.educare.common.models.QuizResults
import com.educare.databinding.FragmentQuizEvaluationBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.wada811.viewbinding.viewBinding


class QuizResultFragment : Fragment(R.layout.fragment_quiz_evaluation) {

    private val binding by viewBinding { FragmentQuizEvaluationBinding.bind(it) }

    private val adapter = QuizResultRecyclerViewAdapter()

    private var questions: List<MCQQuestion>? = null

    private var answers: List<Int>? = null

    private var subject: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        saveResultToServer()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("questions",questions?.toJSON())
        outState.putString("answers",answers?.toJSON())
        outState.putString("subject",subject)
        super.onSaveInstanceState(outState)
    }

    @SuppressLint("SetTextI18n")
    private fun saveResultToServer() {
        val (totalQuestion,correctAns) = getResultData()
        binding.subject.text = "Subject: $subject"
        binding.totalQuestion.text = "Total Questions: $totalQuestion"
        binding.correctAns.text = "Correct Answers: $correctAns"
        adapter.updateItems(questions!!,answers!!)
        val result = QuizResults(subject!!,totalQuestion,correctAns)
        FirebaseFirestore.getInstance()
            .collection("users/${FirebaseAuth.getInstance().currentUser?.phoneNumber}/quiz-results")
            .add(result)
            .addOnSuccessListener {
                makeToast("Your result is saved into server")
            }
            .addOnFailureListener {
                makeToast("Failed to save your result please check your connection")
            }
    }

    private fun getResultData(): Pair<Int, Int> {
        var correctAns = 0
        for((i,e) in questions!!.withIndex()) {
            if (e.ans == answers!![i]) {
                correctAns++
            }
        }
        return questions!!.size to correctAns
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

    private fun initializeProperties() {
        questions = arguments?.getString("questions")?.fromJson()
        answers = arguments?.getString("answers")?.fromJson()
        subject = arguments?.getString("subject")
    }

}
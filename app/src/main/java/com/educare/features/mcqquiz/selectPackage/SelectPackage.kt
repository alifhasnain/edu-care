package com.educare.features.mcqquiz.selectPackage

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.fromJson
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.toJSON
import com.educare.common.models.PackagesStudent
import com.educare.databinding.FragmentSelectPackageBinding
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import timber.log.Timber


class SelectPackage : Fragment(R.layout.fragment_select_package) {

    private val binding by viewBinding { FragmentSelectPackageBinding.bind(it) }

    private var subject: String? = null

    private val adapter = SelectPackageRecyclerAdapter()

    private var chapters: List<String>? = null

    private var collectionPath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchDataFromServer()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("chapters",chapters!!.toJSON())
        outState.putString("subject",subject)
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchDataFromServer() {
        FirebaseFirestore.getInstance()
            .collection("users/${FirebaseAuth.getInstance().currentUser?.phoneNumber}/packages")
            .whereGreaterThanOrEqualTo("expiredAt", Timestamp(System.currentTimeMillis()/1000,0))
            .limit(20)
            .addSnapshotListener { queryDocumentSnapshot, e ->
                if (e != null) {
                    Timber.e(e)
                    makeToast("Error occurred while fetching data.")
                } else {
                    adapter.updateItems(queryDocumentSnapshot!!.map {
                        it.toObject<PackagesStudent>().apply { docID = it.id }
                    })
                }
            }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClicked = {
            findNavController().navigate(
                R.id.action_selectPackage_to_MCQQuizStudent,
                bundleOf(
                    "path" to collectionPath,
                    "chapters" to chapters?.toJSON(),
                    "package" to it.docID,
                    "subject" to subject
                )
            )
        }
    }

    private fun initializeProperties() {
        collectionPath = requireArguments().getString("path")
        subject = arguments?.getString("subject")
        chapters = requireArguments().getString("chapters")!!.fromJson()
    }
}
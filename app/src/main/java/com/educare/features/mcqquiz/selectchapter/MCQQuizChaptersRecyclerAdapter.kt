package com.educare.features.mcqquiz.selectchapter

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.Chapter

class MCQQuizChaptersRecyclerAdapter: SingleItemBaseAdapter<Chapter>() {

    private val chapters = mutableListOf<Chapter>()

    var onCheckChanged: ((Chapter,Boolean) -> Unit)? = null

    fun updateItems(updatedItems: List<Chapter>) {
        chapters.clear()
        chapters.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: Chapter): Boolean {
        return true
    }

    override fun clickedItem(item: Chapter) {

    }

    fun onCheckChanged(item: Chapter, state: Boolean) {
        onCheckChanged?.invoke(item,state)
    }

    override fun getItemForPosition(position: Int): Chapter {
        return chapters[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_chapters_for_mcq_quiz
    }

    override fun getItemCount(): Int {
        return chapters.size
    }
}
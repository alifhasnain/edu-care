package com.educare.features.subjects

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.educare.common.extensions.setValueThenNullify
import com.educare.common.models.Subject
import com.educare.common.repos.GlobalRepository
import kotlinx.coroutines.launch

class SubjectListStudentViewModel: ViewModel() {

    private val repo by lazy { GlobalRepository() }

    private val _subjects: MutableLiveData<List<Subject>> = MutableLiveData()
    val subjects: LiveData<List<Subject>> = _subjects

    val loadingContents: MutableLiveData<Boolean> = MutableLiveData()

    val toast: MutableLiveData<String> = MutableLiveData()

    fun fetchSubjects(path: String) {
        safeScope {
            _subjects.value = repo.fetchSubjects(path)
        }
    }

    private fun safeScope(block: suspend () -> Unit) {
        viewModelScope.launch {
            try {
                loadingContents.value = true
                block()
            } catch (e: Exception) {
                toast.setValueThenNullify(e.message!!)
            } finally {
                loadingContents.value = false
            }
        }
    }

}
package com.educare.features.subjects

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentSubjectListStudentBinding
import com.educare.features.adminpanel.subjects.SubjectsRecyclerViewAdapter
import com.educare.utils.ProfileSharedPrefHelper
import com.google.android.gms.ads.AdRequest


class SubjectListStudent : DataBindingFragment<FragmentSubjectListStudentBinding>(R.layout.fragment_subject_list_student) {

    private val profile
        get() = ProfileSharedPrefHelper.getUserProfile(requireContext())

    private var type: String? = null

    private val adapter by lazy { SubjectsRecyclerViewAdapter() }

    private val viewModel: SubjectListStudentViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewmodel = viewModel
        binding.presenter = this
        binding.adView.loadAd(AdRequest.Builder().build())

        type = arguments?.getString("type")

        initializeRecyclerView()

        fetchSubjects()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeObservers()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("type",type)
        super.onSaveInstanceState(outState)
    }

    fun fetchSubjects() {
        viewModel.fetchSubjects("${profile?.studentClass}")
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner, Observer { makeToast(it) })
        viewModel.subjects.observe(viewLifecycleOwner, Observer {
            adapter.updateSubjects(it)
        })
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClicked = {
            val collectionPath = "${profile?.studentClass}/${it.docID}"
            if (type == "mcq-quiz") {
                findNavController().navigate(
                    R.id.action_subjectListStudent_to_chaptersForMCQQuiz,
                    bundleOf("path" to collectionPath,"subject" to it.name)
                )
            } else {
                findNavController().navigate(
                    R.id.action_subjectListStudent_to_chapterListStudent,
                    bundleOf("path" to collectionPath, "type" to type)
                )
            }
        }
    }
}
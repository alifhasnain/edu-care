package com.educare.features.chapters

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.admanager.AdManager
import com.educare.common.extensions.makeToast
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentChapterListStudentBinding
import com.educare.features.adminpanel.chapters.ChapterListRecyclerViewAdapter
import com.google.android.gms.ads.AdRequest
import timber.log.Timber

class ChapterListStudent : DataBindingFragment<FragmentChapterListStudentBinding>(R.layout.fragment_chapter_list_student) {

    private val adManager by lazy { AdManager.createInstance(requireContext(),lifecycle) }

    private val adapter by lazy { ChapterListRecyclerViewAdapter() }

    private var collectionPath: String? = null

    private var type: String? = null

    private val viewModel by viewModels<ChapterListStudentViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            presenter = this@ChapterListStudent
            viewmodel = viewModel
        }
        binding.adView.loadAd(AdRequest.Builder().build())
        collectionPath = arguments?.getString("path")
        type = arguments?.getString("type")

        initializeRecyclerView()
        fetchChapters()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeObservers()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        outState.putString("type",type)
        super.onSaveInstanceState(outState)
    }

    fun fetchChapters() {
        viewModel.fetchChapters("$collectionPath/chapters")
    }

    private fun navigateToDesiredFragment(docID: String) {
        val navController = findNavController()
        val extraBundle = bundleOf("path" to "$collectionPath/chapters/$docID/$type")
        if (adManager.interstitialAd.isLoaded) {
            adManager.interstitialAd.show()
            adManager.reload()
        } else {
            adManager.reload()
            Timber.e("Ad Not Loaded")
        }
        when (type) {
            "mcq-study" -> {
                navController.navigate(R.id.action_chapterListStudent_to_MCQStudyFragmentStudent, extraBundle)
            }
            "mcq-practice" -> {
                navController.navigate(R.id.action_chapterListStudent_to_MCQPracticeStudent, bundleOf("path" to "$collectionPath/chapters/$docID/mcq-study"))
            }
            "lecture-videos" -> {
                navController.navigate(R.id.action_chapterListStudent_to_lectureVideosFragment, extraBundle)
            }
            "lecture-sheets" -> {
                navController.navigate(R.id.action_chapterListStudent_to_lectureSheetStudent,extraBundle)
            }
        }
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner, Observer { makeToast(it) })
        viewModel.chapters.observe(viewLifecycleOwner, Observer {
            adapter.updateItems(it)
        })
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClicked = {
            navigateToDesiredFragment(it.docID)
        }
    }

}
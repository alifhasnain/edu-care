package com.educare.features.chapters

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.educare.common.extensions.setValueThenNullify
import com.educare.common.models.Chapter
import com.educare.common.repos.GlobalRepository
import kotlinx.coroutines.launch
import timber.log.Timber

class ChapterListStudentViewModel: ViewModel() {

    private val repo by lazy { GlobalRepository() }

    private val _chapters: MutableLiveData<List<Chapter>> = MutableLiveData()
    val chapters: LiveData<List<Chapter>> = _chapters

    val toast: MutableLiveData<String> = MutableLiveData()

    val progressing: MutableLiveData<Boolean> = MutableLiveData()

    fun fetchChapters(path: String) {
        safeScope { _chapters.value = repo.fetchChapters(path) }
    }

    private fun safeScope(block: suspend () -> Unit) {
        viewModelScope.launch {
            try {
                progressing.value = true
                block()
            } catch (e: Exception) {
                Timber.e(e)
                toast.setValueThenNullify("Error occurred while laoding.Please check your connection")
            } finally {
                progressing.value = false
            }
        }
    }

}
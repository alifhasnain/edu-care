package com.educare.features.mcqstudy

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.loadstatefooter.FooterLoadStateAdapter
import com.educare.common.models.DataBindingFragment
import com.educare.common.models.LoadStates
import com.educare.databinding.FragmentMCQStudyStudentBinding
import com.educare.features.adminpanel.mcqstudy.recycleradapter.MCQListRecyclerAdapter
import com.educare.features.adminpanel.mcqstudy.viewmodel.MCQStudyFragmentAdminViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch


class MCQStudyFragmentStudent : DataBindingFragment<FragmentMCQStudyStudentBinding>(R.layout.fragment_m_c_q_study_student) {

    val uiStates: MutableLiveData<LoadStates> = MutableLiveData(LoadStates.Empty())

    private var collectionPath: String? = null

    private val adapter by lazy { MCQListRecyclerAdapter() }

    private val viewModel: MCQStudyFragmentAdminViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectionPath = arguments?.getString("path")
        binding.presenter = this

        binding.stateError.retry.setOnClickListener { adapter.refresh() }

        initializeRecyclerView()
        fetchQuestions()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchQuestions() {
        lifecycleScope.launch {
            viewModel.fetchQuestions("$collectionPath")?.collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
            header = FooterLoadStateAdapter { adapter.retry() },
            footer = FooterLoadStateAdapter { adapter.retry() }
        )

        lifecycleScope.launch {
            adapter.loadStateFlow
                .distinctUntilChanged()
                .collect { compositeLoadStates ->
                    uiStates.value = when (compositeLoadStates.refresh) {
                        is LoadState.NotLoading -> {
                            if (adapter.itemCount <= 0) { LoadStates.Empty("Nothing found") } else { LoadStates.NotEmpty }
                        }
                        is LoadState.Loading -> { LoadStates.Loading }
                        is LoadState.Error -> { LoadStates.Error() }
                    }
                    val errorState = when {
                        compositeLoadStates.append is LoadState.Error -> {
                            compositeLoadStates.append as LoadState.Error
                        }
                        compositeLoadStates.prepend is LoadState.Error -> {
                            compositeLoadStates.prepend as LoadState.Error
                        }
                        else -> null
                    }
                    errorState?.let { makeToast("${it.error.message}") }
                }
        }

        adapter.addLoadStateListener { loadState ->
            if (loadState.refresh !is LoadState.NotLoading) {
                // If we are refreshing data from server then we have to hid
                // the list and show progressbar or in case of an error we have
                // to show the retry button
                takeIf { loadState.refresh is LoadState.Loading }?.let { uiStates.value = LoadStates.Loading }
                takeIf { loadState.refresh is LoadState.Error }?.let { uiStates.value = LoadStates.Error() }
            } else {
                // If not loading data then show the list
                uiStates.value = if (adapter.itemCount <= 0) LoadStates.Empty("Nothing found") else {
                    LoadStates.NotEmpty
                }

                val errorState = when {
                    loadState.append is LoadState.Error -> {
                        loadState.append as LoadState.Error
                    }
                    loadState.prepend is LoadState.Error -> {
                        loadState.prepend as LoadState.Error
                    }
                    else -> null
                }
                errorState?.let { makeToast("${it.error.message}") }
            }
        }
    }


}
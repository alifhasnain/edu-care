package com.educare.features.lecturevideos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.educare.common.extensions.setValueThenNullify
import com.educare.common.models.LectureVideo
import com.educare.common.repos.GlobalRepository
import kotlinx.coroutines.launch
import timber.log.Timber

class LectureVideosViewModel: ViewModel() {

    private val repo by lazy { GlobalRepository() }

    val toast: MutableLiveData<String> = MutableLiveData()

    val progressing: MutableLiveData<Boolean> = MutableLiveData()

    private val _lectureVideos: MutableLiveData<List<LectureVideo>> = MutableLiveData()
    val lectureVideos: LiveData<List<LectureVideo>> = _lectureVideos

    fun fetchLectureVideos(path: String) {
        safeScope { _lectureVideos.value = repo.fetchLectureVideos(path) }
    }

    private fun safeScope(block: suspend () -> Unit) {
        viewModelScope.launch {
            try {
                progressing.value = true
                block()
            } catch (e: Exception) {
                Timber.e(e)
                toast.setValueThenNullify("Error occurred while loading.Please check your connection")
            } finally {
                progressing.value = false
            }
        }
    }
}
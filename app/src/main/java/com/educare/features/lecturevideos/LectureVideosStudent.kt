package com.educare.features.lecturevideos

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentLectureVideosStudentBinding
import com.educare.features.adminpanel.lecturevideos.LectureVideosRecyclerAdapter
import com.educare.features.youtubeplayer.YouTubePlayerActivity


class LectureVideosFragment : DataBindingFragment<FragmentLectureVideosStudentBinding>(R.layout.fragment_lecture_videos_student) {
    
    private var collectionPath: String? = null

    private val viewModel by viewModels<LectureVideosViewModel>()

    private val adapter by lazy { LectureVideosRecyclerAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchLectureVideos()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeObservers()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    fun fetchLectureVideos() {
        viewModel.fetchLectureVideos(collectionPath!!)
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner, Observer { makeToast(it) })
        viewModel.lectureVideos.observe(viewLifecycleOwner, Observer {
            adapter.updateItems(it)
        })
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClicked = {
            startActivity(Intent(activity, YouTubePlayerActivity::class.java).apply {
                putExtras(bundleOf("url" to it.url))
            })
        }
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        binding.apply {
            viewmodel = viewModel
            presenter = this@LectureVideosFragment
        }
    }

}
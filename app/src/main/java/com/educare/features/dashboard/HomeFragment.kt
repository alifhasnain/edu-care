package com.educare.features.dashboard

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.extensions.startActivity
import com.educare.common.extensions.trimmedString
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentHomeBinding
import com.educare.features.adminpanel.AdminPanelActivity
import com.educare.utils.ProfileSharedPrefHelper
import com.educare.utils.ProfileSharedPrefHelper.USER_ADMIN
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import kotlinx.coroutines.*
import org.json.JSONObject
import timber.log.Timber
import java.io.File


class HomeFragment : DataBindingFragment<FragmentHomeBinding>(R.layout.fragment_home) {

    private val navController by lazy { findNavController() }

    private val auth by lazy { FirebaseAuth.getInstance() }

    private val profile
        get() = ProfileSharedPrefHelper.getUserProfile(requireContext())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        binding.presenter = this
        binding.adView.loadAd(AdRequest.Builder().build())

        initializeTitleAndImages()

        loadProfileImageFromFile()

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (ProfileSharedPrefHelper.getUserType(requireContext()) == USER_ADMIN) {
            inflater.inflate(R.menu.home_fragment_admin, menu)
        } else {
            inflater.inflate(R.menu.home_fragment, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.admin_panel -> context?.startActivity<AdminPanelActivity>()
            R.id.profile -> navController.navigate(R.id.action_homeFragment_to_profile2)
            R.id.use_coupon -> useCouponDialog()
            R.id.refer -> navController.navigate(R.id.action_homeFragment_to_referSomeone)
            R.id.sign_out -> {
                AlertDialog.Builder(requireContext()).apply {
                    setTitle("Please verify")
                    setMessage("Do you want to sign out?")
                    setPositiveButton("Yes") { _ , _ ->
                        runWithoutException { auth.signOut() }
                    }
                    setNegativeButton("No",null)
                }.create().show()
            }
        }
        return false
    }

    fun navigateToProfile() {
        navController.navigate(R.id.action_homeFragment_to_profile2)
    }

    fun navigateToSubject(type: String) {
        navController.navigate(R.id.action_homeFragment_to_subjectListStudent, bundleOf("type" to type))
    }

    fun navigateToBuyPackage() {
        navController.navigate(R.id.action_homeFragment_to_buyPackageStudent)
    }

    fun navigateToTransaction() {
        navController.navigate(R.id.action_homeFragment_to_transactionsStudent)
    }

    fun navigateToStudentPackages() {
        navController.navigate(R.id.action_homeFragment_to_packagesStudent)
    }

    fun navigateToQuizResults() {
        navController.navigate(R.id.action_homeFragment_to_quizResultsStudent)
    }

    private fun useCouponDialog() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Type the coupon code")
            val editText = EditText(context).apply {
                hint = "Insert coupon code here"
            }
            setView(editText)
            setPositiveButton("OK") { _,_ ->
                checkCouponFromServer(editText.trimmedString)
                makeToast("Requesting")
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun checkCouponFromServer(coupon: String) {
        val jsonObject = JSONObject()
        jsonObject.put("couponCode",coupon)
        FirebaseFunctions.getInstance("asia-east2")
            .getHttpsCallable("useCoupon")
            .call(jsonObject.toString())
            .addOnSuccessListener {
                makeToast(it.data.toString())
            }
            .addOnFailureListener {
                Timber.e(it)
                makeToast("Error occurred")
            }
    }

    private fun loadProfileImageFromFile() {
        lifecycleScope.launch {
            try {
                val fileDir = requireContext().getExternalFilesDir("profile-photo")
                val imageFile = File(fileDir, profile!!.phoneNo)
                val bitmap = withContext(Dispatchers.IO) { BitmapFactory.decodeFile(imageFile.absolutePath) }
                bitmap?.let { binding.profileImage.setImageBitmap(bitmap) }
            } catch (e: Exception) {
                makeToast(e.message)
            }
        }
    }

    private fun initializeTitleAndImages() {

        binding.userName.text = ProfileSharedPrefHelper.getUserProfile(requireContext())?.name ?: ""

        binding.lectureSheets.title.text = "লেকচার শিট"
        binding.lectureSheets.headerImage.setImageResource(R.drawable.ic_book__pdf)

        binding.lectureVideos.title.text = "লেকচার ভিডিও"
        binding.lectureVideos.headerImage.setImageResource(R.drawable.ic_book_cd)

        binding.mcqStudy.title.text = "MCQ স্টাডি"
        binding.mcqStudy.headerImage.setImageResource(R.drawable.ic_book_2)

        binding.mcqPractice.title.text = "MCQ প্র্যাকটিস"
        binding.mcqPractice.headerImage.setImageResource(R.drawable.ic_book_3)

        binding.mcqQuiz.title.text = "MCQ কুইজ"
        binding.mcqQuiz.headerImage.setImageResource(R.drawable.ic_book_1)

        binding.quizResults.title.text = "কুইজ রেজাল্ট"
        binding.quizResults.headerImage.setImageResource(R.drawable.ic_book_4)

        /*binding.transactions.title.text = "ট্রানজেকশন"
        binding.transactions.headerImage.setImageResource(R.drawable.ic_book_dollar)

        binding.packages.title.text = "প্যাকেজ"
        binding.packages.headerImage.setImageResource(R.drawable.ic_book_5)

        binding.buyPackage.title.text = "প্যাকেজ কিনুন"
        binding.buyPackage.headerImage.setImageResource(R.drawable.ic_book_9)*/

        /*binding.about.title.text = "About"
        binding.about.headerImage.setImageResource(R.drawable.ic_notebook_about)*/
    }

}
package com.educare.features.mcqpractice

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.educare.R
import com.educare.common.models.MCQQuestion
import com.educare.databinding.ListItemMcqPracticeStudentBinding

class MCQPracticeStudentRecyclerAdapter: RecyclerView.Adapter<MCQPracticeStudentRecyclerAdapter.ViewHolder>() {

    private val questions = mutableListOf<MCQQuestion>()

    fun updateItems(updatedItems: List<MCQQuestion>) {
        questions.clear()
        questions.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {

        val binding = ListItemMcqPracticeStudentBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.list_item_mcq_practice_student,parent,false))
        val holder = ViewHolder(binding)

        fun checkCorrectAnsGreen(correctPosition: Int) {
            when (correctPosition) {
                0 -> binding.holder1.setBackgroundResource(R.color.green_light)
                1 -> binding.holder2.setBackgroundResource(R.color.green_light)
                2 -> binding.holder3.setBackgroundResource(R.color.green_light)
                3 -> binding.holder4.setBackgroundResource(R.color.green_light)
            }
        }

        binding.checkbox1.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.holder1.setBackgroundResource(R.color.red_light)
                checkCorrectAnsGreen(questions[holder.absoluteAdapterPosition].ans)
                binding.checkbox2.isChecked = false
                binding.checkbox3.isChecked = false
                binding.checkbox4.isChecked = false
            } else {
                binding.holder1.setBackgroundResource(android.R.color.transparent)
            }
        }

        binding.checkbox2.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.holder2.setBackgroundResource(R.color.red_light)
                checkCorrectAnsGreen(questions[holder.absoluteAdapterPosition].ans)
                binding.checkbox1.isChecked = false
                binding.checkbox3.isChecked = false
                binding.checkbox4.isChecked = false
            } else {
                binding.holder2.setBackgroundResource(android.R.color.transparent)
            }
        }

        binding.checkbox3.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.holder3.setBackgroundResource(R.color.red_light)
                checkCorrectAnsGreen(questions[holder.absoluteAdapterPosition].ans)
                binding.checkbox1.isChecked = false
                binding.checkbox2.isChecked = false
                binding.checkbox4.isChecked = false
            } else {
                binding.holder3.setBackgroundResource(android.R.color.transparent)
            }
        }

        binding.checkbox4.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.holder4.setBackgroundResource(R.color.red_light)
                checkCorrectAnsGreen(questions[holder.absoluteAdapterPosition].ans)
                binding.checkbox1.isChecked = false
                binding.checkbox2.isChecked = false
                binding.checkbox3.isChecked = false
            } else {
                binding.holder4.setBackgroundResource(android.R.color.transparent)
            }
        }
        return holder
    }

    override fun getItemCount(): Int {
        return questions.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(questions[position])
    }

    class ViewHolder(val binding: ListItemMcqPracticeStudentBinding): RecyclerView.ViewHolder(binding.rootLayout) {
        fun bind(question: MCQQuestion) {

            // Clear highlight so it don't affect on view reuse
            binding.holder1.setBackgroundResource(android.R.color.transparent)
            binding.holder2.setBackgroundResource(android.R.color.transparent)
            binding.holder3.setBackgroundResource(android.R.color.transparent)
            binding.holder4.setBackgroundResource(android.R.color.transparent)

            // Clear checkbox so it don't affect on view reuse
            binding.checkbox1.isChecked = false
            binding.checkbox2.isChecked = false
            binding.checkbox3.isChecked = false
            binding.checkbox4.isChecked = false

            binding.question.setDisplayText(question.question)
            binding.choiceOne.setDisplayText(question.choices[0])
            binding.choiceTwo.setDisplayText(question.choices[1])
            binding.choiceThree.setDisplayText(question.choices[2])
            binding.choiceFour.setDisplayText(question.choices[3])
        }
    }
}
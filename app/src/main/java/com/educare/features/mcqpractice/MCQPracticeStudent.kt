package com.educare.features.mcqpractice

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.repos.GlobalRepository
import com.educare.databinding.FragmentMCQPracticeStudentBinding
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.launch
import timber.log.Timber

class MCQPracticeStudent : Fragment(R.layout.fragment_m_c_q_practice_student) {

    private var collectionPath: String? = null

    private val repo = GlobalRepository()

    private val adapter by lazy { MCQPracticeStudentRecyclerAdapter() }

    private val binding by viewBinding { FragmentMCQPracticeStudentBinding.bind(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeViewPager()
        fetchDataFromServer()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path", collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchDataFromServer() {
        lifecycleScope.launch {
            try {
                val questions = repo.fetchMCQQuestions(collectionPath!!)
                if (questions.isNotEmpty()) {
                    binding.next.visibility = View.VISIBLE
                    adapter.updateItems(questions)
                } else {
                    makeToast("No question found")
                }
            } catch (e: Exception) {
                Timber.e(e)
                makeToast("Error occurred while loading question")
            }
        }
    }

    private fun gotoNextQuestion() {
        val currentItem = binding.viewPager.currentItem
        if (currentItem + 2 == adapter.itemCount) {
            binding.next.text = "Finish"
        }
        if (currentItem + 1 == adapter.itemCount) {
            findNavController().navigate(R.id.action_MCQPracticeStudent_to_homeFragment)
        }
        binding.viewPager.currentItem = binding.viewPager.currentItem + 1
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        binding.next.setOnClickListener { gotoNextQuestion() }
    }

    private fun initializeViewPager() {
        binding.viewPager.adapter = adapter
        binding.viewPager.isUserInputEnabled = false
    }
}
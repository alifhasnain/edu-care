package com.educare.features.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.educare.R
import com.educare.common.admanager.AdManager
import com.educare.common.extensions.clearStackAndStartActivity
import com.educare.databinding.ActivityMainBinding
import com.educare.features.signin.SignIn
import com.educare.utils.ProfileSharedPrefHelper
import com.google.android.gms.ads.MobileAds
import com.google.android.material.appbar.AppBarLayout
import com.google.firebase.auth.FirebaseAuth
import com.wada811.databinding.dataBinding

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val binding: ActivityMainBinding by dataBinding()

    private val auth by lazy { FirebaseAuth.getInstance() }

    private val authListener by lazy {
        FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser == null) {
                ProfileSharedPrefHelper.deleteSharedPref(this)
                clearStackAndStartActivity<SignIn>()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MobileAds.initialize(this)
        AdManager.createInstance(this,lifecycle)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        binding.toolbarTitle.text = "Edu Care"
        enableToolbarScrolling(false)

    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(authListener)
    }

    override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(authListener)
    }

    private fun enableToolbarScrolling(enable: Boolean) {
        val params = binding.toolbar.layoutParams as AppBarLayout.LayoutParams
        if (enable) {
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
        } else {
            params.scrollFlags = 0
        }
        binding.toolbar.layoutParams = params
    }
}
package com.educare.features.packages
import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.PackagesStudent

class PackagesStudentRecyclerAdapter: SingleItemBaseAdapter<PackagesStudent>() {

    private val items = mutableListOf<PackagesStudent>()

    fun updateItems(updatedItems: List<PackagesStudent>) {
        items.clear()
        items.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: PackagesStudent): Boolean {
        return true
    }

    override fun clickedItem(item: PackagesStudent) {

    }

    override fun getItemForPosition(position: Int): PackagesStudent {
        return items[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_packages_student
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
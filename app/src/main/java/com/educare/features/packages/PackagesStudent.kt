package com.educare.features.packages

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.models.PackagesStudent
import com.educare.databinding.FragmentPackagesStudentBinding
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.wada811.viewbinding.viewBinding
import timber.log.Timber


class PackagesStudent : Fragment(R.layout.fragment_packages_student) {

    private val binding by viewBinding { FragmentPackagesStudentBinding.bind(it) }

    private val adapter = PackagesStudentRecyclerAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeRecyclerView()
        fetchDataFromServer()
    }

    private fun fetchDataFromServer() {
        FirebaseFirestore.getInstance()
            .collection("users/${FirebaseAuth.getInstance().currentUser?.phoneNumber}/packages")
            .whereGreaterThanOrEqualTo("expiredAt",Timestamp(System.currentTimeMillis()/1000,0))
            .limit(20)
            .addSnapshotListener { queryDocumentSnapshot, e ->
                if (e != null) {
                    Timber.e(e)
                    makeToast("Error occurred while fetching data.")
                } else {
                    adapter.updateItems(queryDocumentSnapshot!!.toObjects(PackagesStudent::class.java))
                }
            }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

}
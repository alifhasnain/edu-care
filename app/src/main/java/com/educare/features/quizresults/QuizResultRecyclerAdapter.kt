package com.educare.features.quizresults

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.QuizResults

class QuizResultRecyclerAdapter: SingleItemBaseAdapter<QuizResults>() {

    private val items = mutableListOf<QuizResults>()

    fun updateItems(updatedItems: List<QuizResults>) {
        items.clear()
        items.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: QuizResults): Boolean {
        return true
    }

    override fun clickedItem(item: QuizResults) {

    }

    override fun getItemForPosition(position: Int): QuizResults {
        return items[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_quiz_result
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
package com.educare.features.quizresults

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.databinding.FragmentQuizResultsStudentBinding
import com.google.android.gms.ads.AdRequest
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObjects
import com.wada811.viewbinding.viewBinding
import timber.log.Timber


class QuizResultsStudent : Fragment(R.layout.fragment_quiz_results_student) {

    private val binding by viewBinding { FragmentQuizResultsStudentBinding.bind(it) }

    private val adapter = QuizResultRecyclerAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.adView.loadAd(AdRequest.Builder().build())
        initializeRecyclerView()
        fetchDataFromServer()
    }

    private fun fetchDataFromServer() {
        FirebaseFirestore.getInstance()
            .collection("/users/${FirebaseAuth.getInstance().currentUser?.phoneNumber}/quiz-results")
            .limit(15)
            .addSnapshotListener { querySnapshot, e ->
                if (e != null) {
                    Timber.e(e)
                    makeToast("Error occurred while loading data")
                } else {
                    adapter.updateItems(querySnapshot!!.toObjects())
                }
            }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }
}
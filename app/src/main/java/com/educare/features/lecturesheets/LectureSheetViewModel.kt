package com.educare.features.lecturesheets

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.educare.common.extensions.setValueThenNullify
import com.educare.common.models.LectureSheet
import com.educare.common.repos.GlobalRepository
import kotlinx.coroutines.launch
import timber.log.Timber

class LectureSheetViewModel: ViewModel() {

    private val repo by lazy { GlobalRepository() }

    private val _lectureSheets: MutableLiveData<List<LectureSheet>> = MutableLiveData()
    val lectureSheets: LiveData<List<LectureSheet>> = _lectureSheets

    val toast: MutableLiveData<String> = MutableLiveData()

    val progressing: MutableLiveData<Boolean> = MutableLiveData()

    fun fetchLectureSheets(path: String) {
        safeScope { _lectureSheets.value = repo.fetchLectureSheets(path) }
    }

    private fun safeScope(block: suspend () -> Unit) {
        viewModelScope.launch {
            try {
                progressing.value = true
                block()
            } catch (e: Exception) {
                Timber.e(e)
                toast.setValueThenNullify("Error occurred while loading.Please check your connection")
            } finally {
                progressing.value = false
            }
        }
    }

}
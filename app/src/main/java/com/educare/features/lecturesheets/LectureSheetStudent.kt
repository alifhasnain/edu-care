package com.educare.features.lecturesheets

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentLectureSheetStudentBinding
import com.educare.features.adminpanel.lecturesheets.LectureSheetsRecyclerAdapter
import com.educare.features.pdfview.PDFViewActivity

class LectureSheetStudent : DataBindingFragment<FragmentLectureSheetStudentBinding>(R.layout.fragment_lecture_sheet_student) {

    private var collectionPath: String? = null

    private val adapter by lazy { LectureSheetsRecyclerAdapter() }

    private val viewModel: LectureSheetViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchLectureSheets()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeObservers()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    fun fetchLectureSheets() {
        viewModel.fetchLectureSheets(collectionPath!!)
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        binding.apply {
            presenter = this@LectureSheetStudent
            viewmodel = viewModel
        }
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner, Observer { makeToast(it) })
        viewModel.lectureSheets.observe(viewLifecycleOwner, Observer { adapter.updateItems(it) })
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClick = {
            startActivity(Intent(activity,PDFViewActivity::class.java).apply {
                putExtras(bundleOf("url" to it.url))
            })
        }
    }
}
package com.educare.features.profile

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.telephony.MbmsDownloadSession.RESULT_CANCELLED
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentProfileBinding
import com.educare.utils.ProfileSharedPrefHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File


private const val SELECT_IMAGE_FROM_STORAGE = 785

class Profile : DataBindingFragment<FragmentProfileBinding>(R.layout.fragment_profile) {

    private val profile
        get() = ProfileSharedPrefHelper.getUserProfile(requireContext())

    private val viewModel by viewModels<ProfileViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        binding.apply {
            presenter = this@Profile
            viewmodel = viewModel
        }

        displayProfileDataToView()

        loadProfileImageFromFile()

        loadProfileData()

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.profile,menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initializeObservers()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELLED) {
            makeToast("Nothing was selected")
        } else if(resultCode == RESULT_OK) {
            saveProfilePhoto(data?.data)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.edit_profile) {
            findNavController().navigate(R.id.action_profile2_to_editProfile)
        }
        return false
    }

    fun selectProfilePhoto() {
        startActivityForResult(Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "image/jpeg"
        }, SELECT_IMAGE_FROM_STORAGE)
    }

    private fun loadProfileImageFromFile() {
        lifecycleScope.launch {
            try {
                val fileDir = requireContext().getExternalFilesDir("profile-photo")
                val imageFile = File(fileDir, profile!!.phoneNo)
                val bitmap = withContext(Dispatchers.IO) { BitmapFactory.decodeFile(imageFile.absolutePath) }
                bitmap?.let { binding.profileImage.setImageBitmap(bitmap) }
            } catch (e: Exception) {
                makeToast(e.message)
            }
        }
    }

    private fun saveProfilePhoto(imageUri: Uri?) {
        lifecycleScope.launch {
            try {
                imageUri?.let {
                    val file = requireContext().getExternalFilesDir("profile-photo")
                    if (!file!!.exists()) {
                        file.mkdir()
                    }
                    withContext(Dispatchers.IO) {
                        File(file,profile!!.phoneNo).writeBytes(readBytesFromUri(it)!!)
                    }
                    loadProfileImageFromFile()
                }
            } catch (e: Exception) {
                makeToast(e.message)
            }
        }
    }

    private fun loadProfileData() {
        viewModel.loadProfileData()
    }

    private fun readBytesFromUri(uri: Uri): ByteArray? = context?.contentResolver?.openInputStream(uri)?.buffered()?.use { it.readBytes() }

    private fun displayProfileDataToView() {
        profile?.let {
            binding.name.text = it.name
            binding.contactNo.text = it.phoneNo
            binding.userClass.text = it.studentClass
        }
    }

    private fun initializeObservers() {

        viewModel.toast.observe(viewLifecycleOwner, Observer {
            makeToast(it)
        })

        viewModel.profile.observe(viewLifecycleOwner, Observer {
            ProfileSharedPrefHelper.saveUserProfile(requireContext(),it)
            displayProfileDataToView()
        })

    }

}
package com.educare.features.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.educare.common.extensions.setValueThenNullify
import com.educare.common.models.User
import com.educare.common.repos.GlobalRepository
import kotlinx.coroutines.launch

class ProfileViewModel: ViewModel() {

    private val repo by lazy { GlobalRepository() }

    private val _profile: MutableLiveData<User> = MutableLiveData()
    val profile: LiveData<User> = _profile

    val refreshing: MutableLiveData<Boolean> = MutableLiveData()

    val toast: MutableLiveData<String> = MutableLiveData()

    fun loadProfileData() {
        refreshing.value = true
        viewModelScope.launch {
            try {
                _profile.value = repo.fetchCurrentUserProfile()
            } catch (e: Exception) {
                toast.setValueThenNullify(e.message!!)
            } finally {
                refreshing.value = false
            }
        }
    }

}
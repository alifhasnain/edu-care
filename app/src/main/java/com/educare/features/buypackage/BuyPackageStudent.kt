package com.educare.features.buypackage

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentBuyPackageStudentBinding


class BuyPackageStudent : DataBindingFragment<FragmentBuyPackageStudentBinding>(R.layout.fragment_buy_package_student) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
    }

    fun showAmountDialog(amount: String) {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Alert")
            setMessage("Please make the bKash transaction of TAKA $amount at +8801813298593 then admin will automatically verify your transaction")
            setPositiveButton("COPY NUMBER") { _,_ ->
                val clipboardmanager =  requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipdata = ClipData.newPlainText("token","+8801813298593")
                clipboardmanager.setPrimaryClip(clipdata)
                makeToast("copied")
            }
            setNegativeButton("cancel",null)
        }.create().show()
    }

}
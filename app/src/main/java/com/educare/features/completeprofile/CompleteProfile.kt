package com.educare.features.completeprofile

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.educare.R
import com.educare.common.extensions.clearStackAndStartActivity
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.setActionBarTitle
import com.educare.common.models.DataBindingAppCompatActivity
import com.educare.common.models.User
import com.educare.databinding.ActivityCompleteProfileBinding
import com.educare.features.home.MainActivity
import com.educare.features.signin.SignIn
import com.educare.utils.HelperClass
import com.educare.utils.ProfileSharedPrefHelper
import com.educare.utils.ProfileSharedPrefHelper.USER_STUDENT
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import timber.log.Timber

class CompleteProfile : DataBindingAppCompatActivity<ActivityCompleteProfileBinding>(R.layout.activity_complete_profile) {

    private val auth by lazy { FirebaseAuth.getInstance() }

    private val db by lazy { Firebase.firestore }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.presenter = this

        setSupportActionBar(binding.toolbar)
        setActionBarTitle("Complete Profile")

        binding.userClass.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                hideKeyboard()
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                hideKeyboard()
            }

        }

        initializeSpinners()
    }

    fun saveProfile() {
        val name = binding.name.editText?.text.toString()
        val userClass = binding.userClass.spinner.selectedItem.toString()
        val contactNo = auth.currentUser?.phoneNumber
        if(checkCredentials(name,contactNo)) {
            val user = User(name, contactNo!!, userClass)
            db.document("/users/$contactNo")
                .set(user).addOnSuccessListener {
                    ProfileSharedPrefHelper.saveUserProfile(this,user)
                    ProfileSharedPrefHelper.saveUserType(this,USER_STUDENT)
                    makeToast("Saved")
                    clearStackAndStartActivity<MainActivity>()
                }.addOnFailureListener {
                    Timber.e(it)
                    makeToast(it.message)
                }
        }

    }

    private fun checkCredentials(name: String,contactNo: String?): Boolean {
        return when {
            name.isEmpty() -> {
                binding.name.error = "Name can't be empty"
                false
            }
            contactNo.isNullOrEmpty() -> {
                makeToast("Please sign in again")
                clearStackAndStartActivity<SignIn>()
                false
            }
            else -> {
                true
            }
        }
    }

    private fun hideKeyboard() {
        val view: View? = this.currentFocus
        if (view != null) {
            val imm: InputMethodManager? = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun initializeSpinners() {
        binding.userClass.title.text = "Class"
        binding.userClass.spinner.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getClasses()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }
}
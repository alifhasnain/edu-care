package com.educare.features.editprofile

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.extensions.trimmedString
import com.educare.common.models.User
import com.educare.databinding.FragmentEditProfileBinding
import com.educare.utils.HelperClass
import com.educare.utils.ProfileSharedPrefHelper
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import timber.log.Timber

class EditProfile : Fragment(R.layout.fragment_edit_profile) {

    private val binding by viewBinding { FragmentEditProfileBinding.bind(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.save.setOnClickListener { saveProfileToServer() }
        initSnapshotListener()
        initializeSpinners()
        initializeViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    private fun initSnapshotListener() {
        FirebaseFirestore.getInstance()
            .document("users/${FirebaseAuth.getInstance().currentUser?.phoneNumber}")
            .addSnapshotListener(requireActivity()) { documentSnapshot, e ->
                runWithoutException {
                    if (e != null || documentSnapshot == null || !documentSnapshot.exists()) {
                        Timber.e(e)
                    } else {
                        ProfileSharedPrefHelper.saveUserProfile(requireContext(),documentSnapshot.toObject<User>()!!)
                        initializeViews()
                    }
                }
            }
    }

    private fun saveProfileToServer() {
        FirebaseFirestore.getInstance()
            .document("users/${FirebaseAuth.getInstance().currentUser?.phoneNumber}")
            .set(User(binding.name.trimmedString,FirebaseAuth.getInstance().currentUser?.phoneNumber!!,binding.classes.selectedItem.toString()))
            .addOnSuccessListener {
                makeToast("saved")
            }
            .addOnFailureListener {
                makeToast("Failed to save data")
            }
    }

    private fun initializeViews() {
        val profile = ProfileSharedPrefHelper.getUserProfile(requireContext())
        binding.name.setText(profile?.name)
        binding.phoneNo.setText(profile?.phoneNo)
        binding.classes.setSelection(
            (binding.classes.adapter as ArrayAdapter<String>).getPosition(profile?.studentClass)
        )
    }

    private fun initializeSpinners() {
        binding.classes.apply {
            adapter = ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                HelperClass.getClasses()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }

}
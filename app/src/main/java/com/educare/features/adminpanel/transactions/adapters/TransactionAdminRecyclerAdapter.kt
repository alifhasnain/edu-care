package com.educare.features.adminpanel.transactions.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.educare.common.models.TransactionDetails
import com.educare.databinding.ListItemTransactionAdminBinding
import com.educare.utils.HelperClass

class TransactionAdminRecyclerAdapter: PagingDataAdapter<TransactionDetails, RecyclerView.ViewHolder>(TRANSACTION_COMPARATOR) {

    var onItemClick: ((TransactionDetails) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListItemTransactionAdminBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        val holder = ViewHolder(binding)
        binding.grantToken.setOnClickListener {
            onItemClick?.invoke(getItem(holder.absoluteAdapterPosition)!!)
        }
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(getItem(position)!!)
    }

    class ViewHolder(val binding: ListItemTransactionAdminBinding): RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(item: TransactionDetails) {
            binding.transactionId.text = "Transaction ID : ${item.trxID}"
            binding.contactNo.text = "Transaction Contact No : ${item.contactNo}"
            binding.userContactNo.text = "User Contact No : ${item.userContact}"
            binding.status.text = item.status
            binding.status.setChipBackgroundColorResource(HelperClass.getColorResourceFromStatus(item.status))
        }
    }

    companion object {
        private val TRANSACTION_COMPARATOR = object : DiffUtil.ItemCallback<TransactionDetails>() {
            override fun areItemsTheSame(
                oldItem: TransactionDetails,
                newItem: TransactionDetails
            ): Boolean {
                return oldItem.trxID == newItem.trxID
            }

            override fun areContentsTheSame(
                oldItem: TransactionDetails,
                newItem: TransactionDetails
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
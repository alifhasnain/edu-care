package com.educare.features.adminpanel.transactions.source

import androidx.paging.PagingSource
import com.educare.common.models.TransactionDetails
import com.educare.common.repos.GlobalRepository
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.toObject
import timber.log.Timber

class TransactionDetailsPagingSource(private val path: String): PagingSource<DocumentSnapshot, TransactionDetails>() {

    private val repo by lazy { GlobalRepository() }

    override suspend fun load(params: LoadParams<DocumentSnapshot>): LoadResult<DocumentSnapshot, TransactionDetails> {
        try {
            val position = params.key
            if (position == null) {
                val transactionsQuerySnapshots = repo.fetchTransactionDetails(path,params.loadSize)
                val lastSnapshot = if(transactionsQuerySnapshots.size()<=0) {
                    null
                } else {
                    transactionsQuerySnapshots.documents[transactionsQuerySnapshots.size()-1]
                }
                val transactionDetails = transactionsQuerySnapshots.map { it.toObject<TransactionDetails>() }

                return LoadResult.Page(
                    data = transactionDetails,
                     prevKey = null,
                    nextKey = lastSnapshot
                )
            } else {
                val transactionsQuerySnapshots = repo.fetTransactionDetailsAfter(path,params.loadSize,position)
                val nextSnapshotKey = if(transactionsQuerySnapshots.size()<=0) {
                    null
                } else {
                    transactionsQuerySnapshots.documents[transactionsQuerySnapshots.size()-1]
                }
                val transactionDetails = transactionsQuerySnapshots.map { it.toObject<TransactionDetails>() }
                return LoadResult.Page(
                    data = transactionDetails,
                    prevKey = position,
                    nextKey = nextSnapshotKey
                )
            }
        } catch (e: Exception) {
            Timber.e(e)
            return LoadResult.Error(e)
        }
    }
}
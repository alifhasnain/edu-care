package com.educare.features.adminpanel.mcqstudy.ui

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.loadstatefooter.FooterLoadStateAdapter
import com.educare.common.models.DataBindingFragment
import com.educare.common.models.LoadStates
import com.educare.common.models.MCQQuestion
import com.educare.databinding.FragmentMCQStudyAdminBinding
import com.educare.features.adminpanel.mcqstudy.recycleradapter.MCQListRecyclerAdapter
import com.educare.features.adminpanel.mcqstudy.viewmodel.MCQStudyFragmentAdminViewModel
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch


class MCQStudyFragmentAdmin : DataBindingFragment<FragmentMCQStudyAdminBinding>(R.layout.fragment_m_c_q_study_admin) {

    val uiStates: MutableLiveData<LoadStates> = MutableLiveData(LoadStates.Empty())

    private var collectionPath: String? = null

    private val adapter by lazy { MCQListRecyclerAdapter() }

    private var questionFetchJob: Job? = null

    private val viewModel: MCQStudyFragmentAdminViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeProperties()

        initializeRecyclerView()

        fetchQuestions()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeObservers()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchQuestions() {
        questionFetchJob?.cancel()
        questionFetchJob = lifecycleScope.launch {
            viewModel.fetchQuestions(collectionPath!!)?.collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun addQuestion() {
        findNavController().navigate(R.id.action_MCQStudyFragmentAdmin_to_addMCQ, bundleOf("path" to collectionPath))
    }

    private fun deleteQuestionFromDB(docID: String) {
        viewModel.deleteQuestion(collectionPath!!,docID)
    }

    private fun showActionDialog(question: MCQQuestion) {
        AlertDialog.Builder(context).apply {
            setItems(arrayOf("Edit","Delete")) { _, position ->
                if(position==0) {
                    findNavController().navigate(
                        R.id.action_MCQStudyFragmentAdmin_to_editMCQAdmin,
                        bundleOf("question" to convertQuestionToJSON(question),"path" to collectionPath)
                    )
                } else if (position == 1) {
                    deleteQuestionFromDB(question.docID)
                }
            }
        }.create().show()
    }

    private fun convertQuestionToJSON(question: MCQQuestion): String? {
        val adapter = Moshi.Builder().build().adapter(MCQQuestion::class.java)
        return adapter.toJson(question)
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner, Observer { makeToast(it) })
    }

    private fun initializeRecyclerView() {

        binding.recyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
            header = FooterLoadStateAdapter { adapter.retry() },
            footer = FooterLoadStateAdapter { adapter.retry() }
        )

        adapter.onLongClick = { showActionDialog(it) }

        lifecycleScope.launch {
            adapter.loadStateFlow
                .distinctUntilChanged()
                .collect { compositeLoadStates ->
                    uiStates.value = when (compositeLoadStates.refresh) {
                        is LoadState.NotLoading -> {
                            if (adapter.itemCount <= 0) { LoadStates.Empty("Nothing found") } else { LoadStates.NotEmpty }
                        }
                        is LoadState.Loading -> { LoadStates.Loading }
                        is LoadState.Error -> { LoadStates.Error() }
                    }
                    val errorState = when {
                        compositeLoadStates.append is LoadState.Error -> {
                            compositeLoadStates.append as LoadState.Error
                        }
                        compositeLoadStates.prepend is LoadState.Error -> {
                            compositeLoadStates.prepend as LoadState.Error
                        }
                        else -> null
                    }
                    errorState?.let { makeToast("${it.error.message}") }
                }
        }
    }

    private fun initializeProperties() {

        binding.presenter = this

        collectionPath = arguments?.getString("path")

        binding.addQuestion.setOnClickListener { addQuestion() }

        binding.stateError.retry.setOnClickListener { adapter.refresh() }

        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launch {
                adapter.refresh()
                delay(1500)
                binding.swipeRefresh.isRefreshing = false
            }
        }
    }
}
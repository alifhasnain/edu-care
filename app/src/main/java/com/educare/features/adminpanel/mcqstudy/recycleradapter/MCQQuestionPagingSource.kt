package com.educare.features.adminpanel.mcqstudy.recycleradapter

import androidx.paging.PagingSource
import com.educare.common.models.MCQQuestion
import com.educare.common.repos.GlobalRepository
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.toObject
import timber.log.Timber


class MCQQuestionPagingSource(private val path: String): PagingSource<DocumentSnapshot, MCQQuestion>() {

    private val repo by lazy { GlobalRepository() }

    override suspend fun load(params: LoadParams<DocumentSnapshot>): LoadResult<DocumentSnapshot, MCQQuestion> {
        try {

            val position = params.key

            return if (position == null) {
                val questionsSnapshots = repo.fetchMCQQuestions(path, params.loadSize.toLong())
                val lastSnapshot = if(questionsSnapshots.size()<=0) {
                    null
                } else {
                    questionsSnapshots.documents[questionsSnapshots.size()-1]
                }

                val questions = questionsSnapshots.map {
                    it.toObject<MCQQuestion>().apply { docID = it.id }
                }

                LoadResult.Page(
                    data = questions,
                    prevKey = null,
                    nextKey = lastSnapshot
                )
            } else {
                val questionsSnapshots = repo.fetchMCQQuestionsAfter(path, params.loadSize.toLong() ,params.key!!)
                val lastSnapshot = if(questionsSnapshots.size()<=0) {
                    null
                } else {
                    questionsSnapshots.documents[questionsSnapshots.size()-1]
                }

                val questions = questionsSnapshots.map {
                    it.toObject<MCQQuestion>().apply { docID = it.id }
                }
                LoadResult.Page(
                    data = questions,
                    prevKey = params.key,
                    nextKey = lastSnapshot
                )
            }
        } catch (e: Exception) {
            Timber.e(e)
            return LoadResult.Error(e)
        }
    }
}
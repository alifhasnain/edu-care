package com.educare.features.adminpanel.availableclasses

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter

class AvailableClassRecyclerViewAdapter : SingleItemBaseAdapter<String>() {

    private val availableClasses = mutableListOf<String>()

    var onItemClicked: ((String) -> Unit)? = null

    fun updateItems(updatedClasses: MutableList<String>) {
        availableClasses.clear()
        availableClasses.addAll(updatedClasses)
        notifyDataSetChanged()
    }

    override fun clickedItem(item: String) {
        onItemClicked?.invoke(item)
    }

    override fun getItemForPosition(position: Int): String {
        return availableClasses[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_available_classes
    }

    override fun getItemCount(): Int {
        return availableClasses.size
    }

    override fun onLongClicked(item: String): Boolean {
        return true
    }

}
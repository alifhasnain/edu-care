package com.educare.features.adminpanel.lecturevideos

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.LectureVideo

class LectureVideosRecyclerAdapter: SingleItemBaseAdapter<LectureVideo>() {

    private val lectureVideos = mutableListOf<LectureVideo>()

    var onItemClicked: ((LectureVideo) -> Unit)? = null

    var onLongClicked: ((LectureVideo) -> Unit)? = null

    fun updateItems(updatedItems: List<LectureVideo>) {
        lectureVideos.clear()
        lectureVideos.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: LectureVideo): Boolean {
        onLongClicked?.invoke(item)
        return true
    }

    override fun clickedItem(item: LectureVideo) {
        onItemClicked?.invoke(item)
    }

    override fun getItemForPosition(position: Int): LectureVideo {
        return lectureVideos[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_lecture_videos
    }

    override fun getItemCount(): Int {
        return lectureVideos.size
    }
}
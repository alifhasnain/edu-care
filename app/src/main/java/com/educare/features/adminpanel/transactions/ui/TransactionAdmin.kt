package com.educare.features.adminpanel.transactions.ui

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.toVisibility
import com.educare.common.loadstatefooter.FooterLoadStateAdapter
import com.educare.databinding.FragmentTransactionAdminBinding
import com.educare.features.adminpanel.transactions.adapters.TransactionAdminRecyclerAdapter
import com.educare.features.adminpanel.transactions.viewmodel.TransactionAdminViewModel
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class TransactionAdmin : Fragment(R.layout.fragment_transaction_admin) {

    private val binding by viewBinding { FragmentTransactionAdminBinding.bind(it) }

    private val viewModel by viewModels<TransactionAdminViewModel>()

    private val adapter = TransactionAdminRecyclerAdapter()

    private var job: Job? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerAdapter()
        fetchDataFromServer()
    }

    private fun fetchDataFromServer() {
        job?.cancel()
        job = lifecycleScope.launch {
            viewModel.loadTransactionDetailsStream("transactions")?.collectLatest {
                adapter.submitData(it)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeObservers()
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner, Observer { makeToast(it) })
    }

    private fun initializeRecyclerAdapter() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
            header = FooterLoadStateAdapter { adapter.retry() },
            footer = FooterLoadStateAdapter { adapter.retry() }
        )
        adapter.onItemClick = {
            findNavController().navigate(
                R.id.action_transactionAdmin_to_approveTransactionAdmin,
                bundleOf("trxID" to it.trxID, "phoneNo" to it.userContact)
            )
        }
        adapter.addLoadStateListener { loadState ->
            if (loadState.refresh !is LoadState.NotLoading) {
                // If we are refreshing data from server then we have to hide
                // the list and show progressbar or in case of an error we have
                // to show the retry button
                binding.recyclerView.visibility = View.GONE
                binding.loadingContent.visibility = toVisibility(loadState.refresh is LoadState.Loading)
                binding.retry.visibility = toVisibility(loadState.refresh is LoadState.Error)
            } else {
                // If not loading data then show the list
                binding.recyclerView.visibility = View.VISIBLE
                binding.loadingContent.visibility = View.GONE
                binding.retry.visibility = View.GONE

                val errorState = when {
                    loadState.append is LoadState.Error -> {
                        loadState.append as LoadState.Error
                    }
                    loadState.prepend is LoadState.Error -> {
                        loadState.prepend as LoadState.Error
                    }
                    else -> null
                }
                errorState?.let { makeToast("${it.error.message}") }
            }
        }
    }

    private fun initializeProperties() {

        binding.retry.setOnClickListener { adapter.refresh() }

        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launch {
                adapter.refresh()
                delay(1000)
                binding.swipeRefresh.isRefreshing = false
            }
        }
    }

}
package com.educare.features.adminpanel

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.educare.R
import com.educare.databinding.ActivityAdminPanelBinding
import com.wada811.viewbinding.viewBinding

class AdminPanelActivity : AppCompatActivity(R.layout.activity_admin_panel) {

    private val binding by viewBinding { ActivityAdminPanelBinding.bind(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false);
        binding.toolbarTitle.text = "Admin Panel"
    }

}
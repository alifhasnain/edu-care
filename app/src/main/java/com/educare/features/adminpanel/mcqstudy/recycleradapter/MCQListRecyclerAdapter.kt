package com.educare.features.adminpanel.mcqstudy.recycleradapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.educare.R
import com.educare.common.models.MCQQuestion
import com.educare.databinding.ListItemMcqStudyAdminBinding

class MCQListRecyclerAdapter: PagingDataAdapter<MCQQuestion,RecyclerView.ViewHolder>(QUESTION_COMPARATOR) {

    var onLongClick: ((MCQQuestion) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ListItemMcqStudyAdminBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_item_mcq_study_admin,
            parent,
            false
        )

        val holder = ViewHolder(binding)

        binding.rootLayout.setOnLongClickListener {
            onLongClick?.invoke(getItem(holder.absoluteAdapterPosition)!!)
            true
        }

        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(getItem(position)!!)
    }

    class ViewHolder(val binding: ListItemMcqStudyAdminBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MCQQuestion) {
            binding.radio1.isChecked = false
            binding.radio2.isChecked = false
            binding.radio3.isChecked = false
            binding.radio4.isChecked = false

            when(item.ans) {
                0 -> binding.radio1.isChecked = true
                1 -> binding.radio2.isChecked = true
                2 -> binding.radio3.isChecked = true
                3 -> binding.radio4.isChecked = true
            }

            binding.question.setDisplayText("${absoluteAdapterPosition+1}. ${item.question}")
            binding.choiceOne.setDisplayText(item.choices[0])
            binding.choiceTwo.setDisplayText(item.choices[1])
            binding.choiceThree.setDisplayText(item.choices[2])
            binding.choiceFour.setDisplayText(item.choices[3])
        }
    }

    companion object {
        private val QUESTION_COMPARATOR = object : DiffUtil.ItemCallback<MCQQuestion>() {

            override fun areItemsTheSame(oldItem: MCQQuestion, newItem: MCQQuestion): Boolean = oldItem.docID == newItem.docID

            override fun areContentsTheSame(oldItem: MCQQuestion, newItem: MCQQuestion): Boolean = oldItem == newItem

        }
    }

}


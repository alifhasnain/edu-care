package com.educare.features.adminpanel.generatecoupon

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.trimmedString
import com.educare.common.models.TokenForServer
import com.educare.databinding.FragmentCreateCouponBinding
import com.google.firebase.functions.FirebaseFunctions
import com.squareup.moshi.Moshi
import com.wada811.viewbinding.viewBinding
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


class CreateCoupon : Fragment(R.layout.fragment_create_coupon) {

    private var selectedDate = Calendar.getInstance().apply { timeInMillis = 0 }

    private var finalDate: Calendar? = null

    private val dateFormatter = SimpleDateFormat("EEE, d MMM, yyyy")

    private val binding by viewBinding { FragmentCreateCouponBinding.bind(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeSpinners()
    }

    private fun createCoupon() {
        if (!checkFields()) {
            return
        }
        binding.createCoupon.isEnabled = false
        val token = TokenForServer(
            binding.code.editText!!.trimmedString,
            binding.useableCount.editText!!.trimmedString.toFloat().toInt(),
            binding.noOfExam.editText!!.trimmedString.toFloat().toInt(),
            finalDate!!.timeInMillis/1000,
            binding.expirationAfterActivation.spinner.selectedItem.toString().toInt()
        )

        val adapter = Moshi.Builder().build().adapter(TokenForServer::class.java)

        FirebaseFunctions.getInstance("asia-east2").getHttpsCallable("createToken")
            .call(adapter.toJson(token))
            .addOnSuccessListener {
                makeToast(it.data.toString())
                binding.createCoupon.isEnabled = true
            }
            .addOnFailureListener {
                Timber.e(it)
                makeToast(it.message)
                binding.createCoupon.isEnabled = true
            }
    }

    private fun checkFields(): Boolean {
        if (binding.code.editText!!.text.isEmpty() || binding.useableCount.editText!!.text.isEmpty() || binding.noOfExam.editText!!.text.isEmpty() || finalDate == null) {
            makeToast("All field must be assigned")
            return false
        }
        return true
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePicker = DatePickerDialog(requireActivity(), DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDayOfMonth ->
            selectedDate.set(selectedYear, selectedMonth, selectedDayOfMonth)
            finalDate = selectedDate
            binding.selectDate.text = dateFormatter.format(selectedDate.time)
        }, year, month, day)

        datePicker.show()
    }

    private fun initializeProperties() {
        binding.selectDate.setOnClickListener { showDatePicker() }
        binding.createCoupon.setOnClickListener { createCoupon() }
    }

    private fun initializeSpinners() {
        binding.expirationAfterActivation.title.text = "Day available after activation"
        val day = mutableListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30)
        binding.expirationAfterActivation.spinner.apply {
            adapter = ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                day
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }

}
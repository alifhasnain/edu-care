package com.educare.features.adminpanel.mcqstudy.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.educare.common.extensions.setValueThenNullify
import com.educare.common.models.MCQQuestion
import com.educare.common.repos.GlobalRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class MCQStudyFragmentAdminViewModel: ViewModel() {

    val toast: MutableLiveData<String> = MutableLiveData()

    private val repository by lazy { GlobalRepository() }

    private var lastCollectionPath: String? = null

    private var currentSearchResult: Flow<PagingData<MCQQuestion>>? = null

    fun fetchQuestions(path: String): Flow<PagingData<MCQQuestion>>? {
        val lastResult = currentSearchResult
        return if (path == lastCollectionPath && lastResult!=null) {
            lastResult
        } else {
            lastCollectionPath = path
            val newResult: Flow<PagingData<MCQQuestion>> = repository.getMCQQuestionStream(path).cachedIn(viewModelScope)
            currentSearchResult = newResult
            newResult
        }
    }

    fun deleteQuestion(path: String,docID: String) {
        safeScope {
            repository.deleteSingleMCQQuestion(path,docID)
            toast.setValueThenNullify("Deleted")
        }
    }

    private fun safeScope(block: suspend () -> Unit) = viewModelScope.launch {
        try {
            toast.setValueThenNullify("Processing please wait")
            block()
        } catch (e: Exception) {
            toast.setValueThenNullify("Error occurred please check your connection.")
        }
    }
}
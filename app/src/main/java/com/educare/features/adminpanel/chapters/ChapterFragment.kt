package com.educare.features.adminpanel.chapters

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.core.view.updateMargins
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.dp
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.extensions.trimmedString
import com.educare.common.models.Chapter
import com.educare.common.models.ViewBindingFragment
import com.educare.databinding.FragmentChapterBinding
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber


class ChapterFragment : ViewBindingFragment<FragmentChapterBinding>(R.layout.fragment_chapter, { FragmentChapterBinding.bind (it) }) {

    private val db by lazy { FirebaseFirestore.getInstance() }

    private val adapter by lazy { ChapterListRecyclerViewAdapter() }

    private var collectionPath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectionPath = arguments?.getString("path")

        initializeRecyclerView()

        fetchChaptersFromServer()

        binding.addChapter.setOnClickListener { addChapter() }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun addChapter() {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL

        val chapterName = EditText(context)
        chapterName.hint = "Chapter Name"
        layout.addView(chapterName)

        val chapterNo = EditText(context)
        chapterNo.inputType = InputType.TYPE_CLASS_NUMBER
        chapterNo.hint = "Chapter No"
        layout.addView(chapterNo)

        (chapterName.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,12.dp,8.dp,4.dp)
        }
        (chapterNo.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,4.dp,8.dp,12.dp)
        }

        AlertDialog.Builder(requireContext()).apply {
            setTitle("Type the chapter name")
            setView(layout)
            setPositiveButton("OK") { _,_ ->
                pushChapterToDB(Chapter(chapterName.trimmedString,chapterNo.trimmedString.toFloatOrNull() ?: 0f))
                makeToast("Please wait while adding data")
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun pushChapterToDB(chapter: Chapter) {
        if(chapter.name.isEmpty()) {
            makeToast("Chapter name can't be empty")
            return
        }
        db.collection("$collectionPath/chapters")
            .add(chapter)
            .addOnSuccessListener {
                makeToast("Chapter added successfully")
            }.addOnFailureListener {
                Timber.e(it)
                makeToast(it.message)
            }
    }

    private fun deleteChapter(chapter: Chapter) {
        AlertDialog.Builder(context).apply {
            setItems(arrayOf("Delete")) { _, _ ->
                deleteChapterFromDB(chapter)
            }
        }.create().show()
    }

    private fun deleteChapterFromDB(chapter: Chapter) {
        fun delete(chapter: Chapter) {
            db.document("$collectionPath/chapters/${chapter.docID}")
                .delete()
                .addOnSuccessListener { makeToast("Deleted successfully") }
                .addOnFailureListener {
                    Timber.e(it)
                    makeToast(it.message)
                }
        }
        AlertDialog.Builder(context).apply {
            setTitle("Are you sure?")
            setMessage("Are you sure you want to delete ${chapter.name}")
                .setPositiveButton("Yes") { _,_ ->
                    delete(chapter)
                }
                .setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun fetchChaptersFromServer() {
        FirebaseFirestore.getInstance().collection("$collectionPath/chapters")
            .orderBy("chapterNo")
            .addSnapshotListener(requireActivity()) { querySnapshot, e ->
                runWithoutException {
                    if(e != null) {
                        makeToast("Failed to refresh data. Please check your connection")
                        return@addSnapshotListener
                    }
                    val chapters = mutableListOf<Chapter>()
                    for (doc in querySnapshot!!) {
                        chapters.add(
                            Chapter(
                                doc.getString("name")!!,
                                doc.getDouble("chapterNo")!!.toFloat(),
                                doc.id
                            )
                        )
                    }
                    adapter.updateItems(chapters)
                }
            }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClicked = {
            findNavController().navigate(
                R.id.action_chapterFragment_to_availableQuestionsFragment,
                bundleOf("path" to "$collectionPath/chapters/${it.docID}")
            )
        }
        adapter.onLongClicked = { deleteChapter(it) }
    }

}
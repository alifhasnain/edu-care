package com.educare.features.adminpanel.availableclasses

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.models.ViewBindingFragment
import com.educare.databinding.FragmentAvailableClassesBinding
import com.educare.utils.HelperClass


class AvailableClasses : ViewBindingFragment<FragmentAvailableClassesBinding>(R.layout.fragment_available_classes, { FragmentAvailableClassesBinding.bind (it)}) {

    private val navController by lazy { findNavController() }

    private val adapter by lazy { AvailableClassRecyclerViewAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeRecyclerView()

    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.updateItems(HelperClass.getClasses())
        adapter.onItemClicked = {
            navController.navigate(R.id.action_availableClasses_to_subjectFragment, bundleOf("class" to it))
        }
    }

}
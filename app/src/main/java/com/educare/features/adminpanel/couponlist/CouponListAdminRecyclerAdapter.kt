package com.educare.features.adminpanel.couponlist

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.Token

class CouponListAdminRecyclerAdapter: SingleItemBaseAdapter<Token>() {

    private val tokens = mutableListOf<Token>()

    var onLongClick: ((Token) -> Unit)? = null

    fun updateItems(updatedItems: List<Token>) {
        tokens.clear()
        tokens.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: Token): Boolean {
        onLongClick?.invoke(item)
        return true
    }

    override fun clickedItem(item: Token) {

    }

    override fun getItemForPosition(position: Int): Token {
        return tokens[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_coupon_admin
    }

    override fun getItemCount(): Int {
        return tokens.size
    }
}
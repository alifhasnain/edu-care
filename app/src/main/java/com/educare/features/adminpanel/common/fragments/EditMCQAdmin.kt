package com.educare.features.adminpanel.common.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.trimmedString
import com.educare.common.models.DataBindingFragment
import com.educare.common.models.MCQQuestion
import com.educare.common.repos.GlobalRepository
import com.educare.databinding.FragmentEditMCQAdminBinding
import com.squareup.moshi.Moshi
import kotlinx.coroutines.launch
import timber.log.Timber


class EditMCQAdmin : DataBindingFragment<FragmentEditMCQAdminBinding>(R.layout.fragment_edit_m_c_q_admin) {

    private val repo by lazy { GlobalRepository() }

    private var collectionPath: String? = null

    private var question: MCQQuestion? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        collectionPath = arguments?.getString("path")
        question = getQuestionFromJSON(arguments?.getString("question")!!)

        initializeTitles()
        initializeTextWatchers()
        initializeSpinners()

        populateViewsWithQuestion()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("question",convertQuestionToJSON(question!!))
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    fun saveQuestionToServer() {
        lifecycleScope.launch {
            try {

                if(checkIfFieldIsEmpty()) return@launch

                makeToast("Saving please wait")

                binding.editQuestion.isEnabled = false

                repo.updateSingleMCQQuestion(
                    "$collectionPath/${question?.docID}",
                    MCQQuestion(
                        binding.questionInput.input.trimmedString,
                        mutableListOf(
                            binding.choiceOneInput.input.trimmedString,
                            binding.choiceTwoInput.input.trimmedString,
                            binding.choiceThreeInput.input.trimmedString,
                            binding.choiceFourInput.input.trimmedString
                        ),
                        binding.correctAns.spinner.selectedItem.toString().toInt() - 1
                    )
                )
            } catch (e: Exception) {
                makeToast(e.message)
                Timber.e(e)
            } finally {
                makeToast("saved")
                binding.editQuestion.isEnabled = true
            }
        }
    }

    private fun convertQuestionToJSON(question: MCQQuestion): String? {
        val adapter = Moshi.Builder().build().adapter(MCQQuestion::class.java)
        return adapter.toJson(question)
    }

    private fun getQuestionFromJSON(question: String): MCQQuestion {
        val adapter = Moshi.Builder().build().adapter(MCQQuestion::class.java)
        return adapter.fromJson(question)!!
    }

    private fun checkIfFieldIsEmpty(): Boolean {
        val question = binding.questionInput.input.trimmedString
        val choice1 = binding.choiceOneInput.input.trimmedString
        val choice2 = binding.choiceTwoInput.input.trimmedString
        val choice3 = binding.choiceThreeInput.input.trimmedString
        val choice4 = binding.choiceFourInput.input.trimmedString
        return when {
            question.isEmpty() -> {
                makeToast("Question can't be empty")
                true
            }
            choice1.isEmpty() or choice2.isEmpty() or choice3.isEmpty() or choice4.isEmpty() -> {
                makeToast("Choice can't be empty")
                true
            }
            else -> false
        }
    }

    private fun populateViewsWithQuestion() {
        question?.let {

            binding.questionInput.input.setText(it.question)
            binding.choiceOneInput.input.setText(it.choices[0])
            binding.choiceTwoInput.input.setText(it.choices[1])
            binding.choiceThreeInput.input.setText(it.choices[2])
            binding.choiceFourInput.input.setText(it.choices[3])

            binding.correctAns.spinner.setSelection(it.ans)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initializeTitles() {

        binding.questionInput.title.text = "Question"
        binding.questionInput.input.hint = "Enter your question here"

        binding.formattedQuestion.title.text = "Formatted Question"

        binding.choiceOneInput.title.text = "Enter first choice input"
        binding.choiceOneInput.input.hint = "Input text here"

        binding.choiceOneFormatted.title.text = "Formatted output"

        binding.choiceTwoInput.title.text = "Enter second choice input"
        binding.choiceTwoInput.input.hint = "Input text here"

        binding.choiceTwoFormatted.title.text = "Formatted output"

        binding.choiceThreeInput.title.text = "Enter third choice input"
        binding.choiceThreeInput.input.hint = "Input text here"

        binding.choiceThreeFormatted.title.text = "Formatted output"

        binding.choiceFourInput.title.text = "Enter fourth choice input"
        binding.choiceFourInput.input.hint = "Input text here"

        binding.choiceFourFormatted.title.text = "Formatted output"

        binding.correctAns.title.text = "Correct Answer"

    }

    private fun initializeSpinners() {
        binding.correctAns.spinner.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                arrayOf("1", "2", "3", "4")
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }

    private fun initializeTextWatchers() {

        binding.questionInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.formattedQuestion.description.setDisplayText(text.toString())
            }
        })

        binding.choiceOneInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceOneFormatted.description.setDisplayText(text.toString())
            }
        })

        binding.choiceTwoInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceTwoFormatted.description.setDisplayText(text.toString())
            }
        })

        binding.choiceThreeInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceThreeFormatted.description.setDisplayText(text.toString())
            }
        })

        binding.choiceFourInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceFourFormatted.description.setDisplayText(text.toString())
            }
        })
    }
}
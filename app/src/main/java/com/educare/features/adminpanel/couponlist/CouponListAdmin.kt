package com.educare.features.adminpanel.couponlist

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.models.Token
import com.educare.databinding.FragmentCouponListAdminBinding
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import timber.log.Timber


class CouponListAdmin : Fragment(R.layout.fragment_coupon_list_admin) {

    private val binding by viewBinding { FragmentCouponListAdminBinding.bind(it) }

    private val adapter = CouponListAdminRecyclerAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeRecyclerView()
        fetchDataFromServer()
    }

    private fun fetchDataFromServer() {
        FirebaseFirestore.getInstance().collection("tokens")
            .whereGreaterThanOrEqualTo("expireDate",Timestamp(System.currentTimeMillis()/1000,0))
            .addSnapshotListener(requireActivity()) { querySnapshot, e ->
                runWithoutException {
                    if (e != null) {
                        Timber.e(e)
                        makeToast(e.message)
                    } else {
                        adapter.updateItems(querySnapshot!!.map { it.toObject<Token>() })
                    }
                }
            }
    }

    private fun deleteToken(docID: String) {
        AlertDialog.Builder(context).apply {
            setItems(arrayOf("Delete")) { _, _ ->
                deleteTokenFromDB(docID)
            }
        }.create().show()
    }

    private fun deleteTokenFromDB(docID: String) {
        lifecycleScope.launch {
            try {
                FirebaseFirestore.getInstance().document("/tokens/$docID").delete().await()
                makeToast("Deleted")
            } catch (e: Exception) {
                Timber.e(e)
                makeToast(e.message)
            }
        }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onLongClick = {
            deleteToken(it.couponCode)
        }
    }

}
package com.educare.features.adminpanel.mcqquiz

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.models.MCQQuestion
import com.educare.common.models.MCQQuestionList
import com.educare.databinding.FragmentMCQQuizAdminBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import timber.log.Timber


class MCQQuizAdmin : Fragment(R.layout.fragment_m_c_q_quiz_admin) {

    private var collectionPath: String? = null

    private val binding by viewBinding { FragmentMCQQuizAdminBinding.bind(it) }

    private val adapter by lazy { MCQQuizRecyclerAdapterAdmin() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchQuestionFromServer()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchQuestionFromServer() {
        FirebaseFirestore.getInstance().document("$collectionPath/question1")
            .addSnapshotListener { documentSnapshot, e ->
                e?.let {
                    Timber.e(e)
                    return@addSnapshotListener
                }
                if (documentSnapshot!=null && documentSnapshot.exists()) {
                    val questions = documentSnapshot.toObject<MCQQuestionList>()!!
                    adapter.updateItems(questions)
                } else {
                    Timber.e("Document doesn't exist")
                    makeToast("No document found")
                }
            }
    }

    private fun deleteItem(item: MCQQuestion) {
        AlertDialog.Builder(requireContext()).apply {
            setItems(arrayOf("Delete")) { _, _ -> deleteItemFromDB(item)}
        }.create().show()
    }

    private fun deleteItemFromDB(item: MCQQuestion) {
        GlobalScope.launch {
            try {
                val updatedItems = adapter.questions.filter { it.question != item.question }
                FirebaseFirestore.getInstance()
                    .document("$collectionPath/question1")
                    .set(MCQQuestionList(updatedItems as MutableList<MCQQuestion>))
                    .await()
            } catch (e: Exception) {
                Timber.e(e)
                makeToast("Error occurred while updating data")
            }
        }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onLongClick = {
            deleteItem(it)
        }
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        binding.addQuestion.setOnClickListener {
            findNavController().navigate(R.id.action_MCQQuizAdmin_to_addQuizMCQ, bundleOf("path" to collectionPath))
        }
    }
}
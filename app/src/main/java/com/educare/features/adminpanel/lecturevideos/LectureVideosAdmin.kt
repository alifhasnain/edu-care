package com.educare.features.adminpanel.lecturevideos

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.dp
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.extensions.trimmedString
import com.educare.common.models.LectureVideo
import com.educare.common.repos.GlobalRepository
import com.educare.databinding.FragmentLectureVideosAdminBinding
import com.educare.features.youtubeplayer.YouTubePlayerActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.launch
import timber.log.Timber


class LectureVideosAdmin : Fragment(R.layout.fragment_lecture_videos_admin) {

    private val repo by lazy { GlobalRepository() }

    private val adapter by lazy { LectureVideosRecyclerAdapter() }

    private val binding: FragmentLectureVideosAdminBinding by viewBinding { FragmentLectureVideosAdminBinding.bind(it) }

    private var collectionPath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchLectureVideos()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchLectureVideos() {
        FirebaseFirestore.getInstance().collection(collectionPath!!).addSnapshotListener(requireActivity()) { querySnapshot, e ->
            runWithoutException {
                if (e != null) {
                    Timber.e(e)
                } else {
                    adapter.updateItems(querySnapshot!!.map { it.toObject<LectureVideo>().apply { docID = it.id } })
                }
            }
        }
    }

    private fun addLectureVideo() {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL

        val videoTitle = EditText(context)
        videoTitle.hint = "Video Title"
        layout.addView(videoTitle)

        val videoURL = EditText(context)
        videoURL.hint = "Video URL"
        layout.addView(videoURL)

        (videoTitle.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,12.dp,8.dp,4.dp)
        }
        (videoURL.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,4.dp,8.dp,12.dp)
        }

        AlertDialog.Builder(requireContext()).apply {
            setTitle("Type the chapter name")
            setView(layout)
            setPositiveButton("OK") { _,_ ->
                addLectureVideoToDB(videoTitle.trimmedString,videoURL.trimmedString)
                makeToast("Please wait while adding data")
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun addLectureVideoToDB(title: String, url: String) {
        if(title.isEmpty() || url.isEmpty()) {
            makeToast("Empty text is not allowed")
        } else {
            lifecycleScope.launch {
                try {
                    makeToast("Adding data please wait")
                    repo.addLectureVideo("$collectionPath/", LectureVideo(title, url))
                    makeToast("Added successfully")
                } catch (e: Exception) {
                    Timber.e(e)
                    makeToast(e.message)
                }
            }
        }
    }

    private fun showContextMenu(lectureVideo: LectureVideo) {
        AlertDialog.Builder(context).apply {
            setItems(arrayOf("Edit","Delete")) { _, position ->
                if(position==0) {
                    editLectureVideo(lectureVideo)
                } else if (position == 1) {
                    deleteLectureVideo(lectureVideo.docID)
                }
            }
        }.create().show()
    }

    private fun editLectureVideo(lectureVideo: LectureVideo) {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL

        val videoTitle = EditText(context)
        videoTitle.hint = "Video Title"
        videoTitle.setText(lectureVideo.title)
        layout.addView(videoTitle)

        val videoURL = EditText(context)
        videoURL.hint = "Video URL"
        videoURL.setText(lectureVideo.url)
        layout.addView(videoURL)

        (videoTitle.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,12.dp,8.dp,4.dp)
        }
        (videoURL.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,4.dp,8.dp,12.dp)
        }

        AlertDialog.Builder(requireContext()).apply {
            setTitle("Type the chapter name")
            setView(layout)
            setPositiveButton("OK") { _,_ ->
                saveEditedLectureVideo(LectureVideo(videoTitle.trimmedString,videoURL.trimmedString,lectureVideo.docID))
                makeToast("Please wait while saving data")
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun saveEditedLectureVideo(lectureVideo: LectureVideo) {
        lifecycleScope.launch {
            try {
                repo.saveEditedLectureVideo(collectionPath!!,lectureVideo)
                makeToast("saved")
            } catch (e: Exception) {
                Timber.e(e)
                makeToast(e.message)
            }
        }
    }

    private fun deleteLectureVideo(docID: String) {
        lifecycleScope.launch {
            try {
                repo.deleteLectureVideo(collectionPath!!,docID)
                makeToast("deleted")
            } catch (e: Exception) {
                Timber.e(e)
                makeToast(e.message)
            }
        }
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        binding.addVideo.setOnClickListener { addLectureVideo() }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClicked = {
            startActivity(Intent(activity,YouTubePlayerActivity::class.java).apply {
                putExtras(bundleOf("url" to it.url))
            })
        }
        adapter.onLongClicked = {
            showContextMenu(it)
        }
    }

}
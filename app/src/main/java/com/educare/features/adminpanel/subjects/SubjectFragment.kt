package com.educare.features.adminpanel.subjects

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.extensions.trimmedString
import com.educare.common.models.Subject
import com.educare.common.models.ViewBindingFragment
import com.educare.databinding.FragmentSubjectBinding
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber


class SubjectFragment : ViewBindingFragment<FragmentSubjectBinding>(R.layout.fragment_subject, { FragmentSubjectBinding.bind(it) }) {

    private val db by lazy { FirebaseFirestore.getInstance() }

    private var classLevel: String? = null

    private val adapter by lazy { SubjectsRecyclerViewAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        classLevel = arguments?.getString("class")

        initializeRecyclerView()

        fetchSubjectsFromDB()

        binding.addSubject.setOnClickListener { addNewSubject() }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("class",classLevel)
        super.onSaveInstanceState(outState)
    }

    private fun addNewSubject() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Type the subject name")
            val editText = EditText(context).apply {
                hint = "Insert subject name here"
            }
            setView(editText)
            setPositiveButton("OK") { _,_ ->
                pushSubjectNameToDB(editText.trimmedString)
                makeToast("Please wait while adding data")
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun pushSubjectNameToDB(subjectName: String) {
        if(subjectName.isEmpty()) {
            makeToast("Subject name can't be empty")
            return
        }
        db.collection(classLevel!!).add(Subject(subjectName))
            .addOnSuccessListener {
                makeToast("Subject added successfully")
            }.addOnFailureListener {
                Timber.e(it)
                makeToast(it.message)
            }
    }

    private fun deleteSubject(subject: Subject) {
        AlertDialog.Builder(context).apply {
            setItems(arrayOf("Delete")) { _, _ ->
                deleteSubjectFromDB(subject)
            }
        }.create().show()
    }

    private fun deleteSubjectFromDB(subject: Subject) {
        fun delete(subject: Subject) {
            db.document("$classLevel/${subject.docID}")
                .delete()
                .addOnSuccessListener { makeToast("Deleted successfully") }
                .addOnFailureListener {
                    Timber.e(it)
                    makeToast(it.message)
                }
        }
        AlertDialog.Builder(context).apply {
            setTitle("Are you sure?")
            setMessage("Are you sure you want to delete ${subject.name}")
                .setPositiveButton("Yes") { _,_ ->
                    delete(subject)
                }
                .setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun fetchSubjectsFromDB() {
        db.collection(classLevel!!).addSnapshotListener(requireActivity()) { querySnapshots,e ->
            runWithoutException {
                if(e != null) {
                    makeToast("Failed to refresh data. Please check your connection")
                    return@addSnapshotListener
                }
                val subjects = mutableListOf<Subject>()
                for (doc in querySnapshots!!) {
                    subjects.add(Subject(doc.getString("name")!!,doc.id))
                }
                adapter.updateSubjects(subjects)
            }
        }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClicked = {
            val collectionPath = "$classLevel/${it.docID}"
            findNavController().navigate(R.id.action_subjectFragment_to_chapterFragment, bundleOf("path" to collectionPath))
        }
        adapter.onLongClicked = { deleteSubject(it) }
    }

}
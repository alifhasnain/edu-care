package com.educare.features.adminpanel.subjects

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.Subject

class SubjectsRecyclerViewAdapter: SingleItemBaseAdapter<Subject>() {

    private val subjects = mutableListOf<Subject>()

    var onItemClicked: ((Subject) -> Unit)? = null
    var onLongClicked: ((Subject) -> Unit)? = null

    fun updateSubjects(updatedSubjects: List<Subject>) {
        subjects.clear()
        subjects.addAll(updatedSubjects)
        notifyDataSetChanged()
    }

    override fun clickedItem(item: Subject) {
        onItemClicked?.invoke(item)
    }

    override fun getItemForPosition(position: Int): Subject {
        return subjects[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_subjects
    }

    override fun getItemCount(): Int {
        return subjects.size
    }

    override fun onLongClicked(item: Subject): Boolean {
        onLongClicked?.invoke(item)
        return true
    }
}
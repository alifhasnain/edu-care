package com.educare.features.adminpanel.mcqquiz

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.trimmedString
import com.educare.common.models.MCQQuestion
import com.educare.common.models.MCQQuestionList
import com.educare.databinding.FragmentAddQuizMCQBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import timber.log.Timber

class AddQuizMCQ : Fragment(R.layout.fragment_add_quiz_m_c_q) {

    private val db by lazy { FirebaseFirestore.getInstance() }

    private val binding by viewBinding { FragmentAddQuizMCQBinding.bind(it) }

    private var collectionPath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeTitles()
        initializeTextWatchers()
        initializeSpinners()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun addQuestionToDB() {
        if (checkIfFieldIsEmpty()) { return }
        lifecycleScope.launch {
            try {
                binding.addQuestion.isEnabled = false
                val mcqQuestion = MCQQuestion(
                    binding.questionInput.input.trimmedString,
                    mutableListOf(
                        binding.choiceOneInput.input.trimmedString,
                        binding.choiceTwoInput.input.trimmedString,
                        binding.choiceThreeInput.input.trimmedString,
                        binding.choiceFourInput.input.trimmedString
                    ),
                    binding.correctAns.spinner.selectedItem.toString().toInt() - 1
                )
                val questionsSnapshot = db.document("$collectionPath/question1").get().await()
                if (!questionsSnapshot.exists()) {
                    val updatedQuestions = mutableListOf(mcqQuestion)
                    db.document("$collectionPath/question1").set(MCQQuestionList(updatedQuestions))
                    makeToast("ADDED")
                    clearInputFields()
                } else {
                    val questionList = questionsSnapshot.toObject<MCQQuestionList>()
                    questionList?.questions?.add(mcqQuestion)
                    db.document("$collectionPath/question1").set(questionList!!)
                    makeToast("ADDED")
                    clearInputFields()
                }
            } catch (e: Exception) {
                Timber.e(e)
                makeToast(e.message)
            } finally {
                binding.addQuestion.isEnabled = true
            }
        }
    }

    private fun checkIfFieldIsEmpty(): Boolean {
        val question = binding.questionInput.input.trimmedString
        val choice1 = binding.choiceOneInput.input.trimmedString
        val choice2 = binding.choiceTwoInput.input.trimmedString
        val choice3 = binding.choiceThreeInput.input.trimmedString
        val choice4 = binding.choiceFourInput.input.trimmedString
        return when {
            question.isEmpty() -> {
                makeToast("Question can't be empty")
                true
            }
            choice1.isEmpty() or choice2.isEmpty() or choice3.isEmpty() or choice4.isEmpty() -> {
                makeToast("Choice can't be empty")
                true
            }
            else -> false
        }
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        binding.addQuestion.setOnClickListener { addQuestionToDB() }
    }


    private fun clearInputFields() {
        binding.questionInput.input.setText("")
        binding.choiceOneInput.input.setText("")
        binding.choiceTwoInput.input.setText("")
        binding.choiceThreeInput.input.setText("")
        binding.choiceFourInput.input.setText("")
    }

    @SuppressLint("SetTextI18n")
    private fun initializeTitles() {

        binding.questionInput.title.text = "Question"
        binding.questionInput.input.hint = "Enter your question here"

        binding.formattedQuestion.title.text = "Formatted Question"

        binding.choiceOneInput.title.text = "Enter first choice input"
        binding.choiceOneInput.input.hint = "Input text here"

        binding.choiceOneFormatted.title.text = "Formatted output"

        binding.choiceTwoInput.title.text = "Enter second choice input"
        binding.choiceTwoInput.input.hint = "Input text here"

        binding.choiceTwoFormatted.title.text = "Formatted output"

        binding.choiceThreeInput.title.text = "Enter third choice input"
        binding.choiceThreeInput.input.hint = "Input text here"

        binding.choiceThreeFormatted.title.text = "Formatted output"

        binding.choiceFourInput.title.text = "Enter fourth choice input"
        binding.choiceFourInput.input.hint = "Input text here"

        binding.choiceFourFormatted.title.text = "Formatted output"

        binding.correctAns.title.text = "Correct Answer"

    }

    private fun initializeSpinners() {
        binding.correctAns.spinner.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                arrayOf("1", "2", "3", "4")
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }

    private fun initializeTextWatchers() {

        binding.questionInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.formattedQuestion.description.setDisplayText(text.toString())
            }
        })

        binding.choiceOneInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceOneFormatted.description.setDisplayText(text.toString())
            }
        })

        binding.choiceTwoInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceTwoFormatted.description.setDisplayText(text.toString())
            }
        })

        binding.choiceThreeInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceThreeFormatted.description.setDisplayText(text.toString())
            }
        })

        binding.choiceFourInput.input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.choiceFourFormatted.description.setDisplayText(text.toString())
            }
        })
    }
}
package com.educare.features.adminpanel.transactionapproval

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.databinding.FragmentApproveTransactionAdminBinding
import com.educare.utils.HelperClass
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.wada811.viewbinding.viewBinding
import timber.log.Timber
import java.util.*


class ApproveTransactionAdmin : Fragment(R.layout.fragment_approve_transaction_admin) {

    private val binding by viewBinding { FragmentApproveTransactionAdminBinding.bind(it) }

    private val db = FirebaseFirestore.getInstance()

    private var trxID: String? = null

    private var userContact: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("trxID",trxID)
        outState.putString("phoneNo",userContact)
        super.onSaveInstanceState(outState)
    }

    fun threeDays20xm() {
        val transactionDocRef = db.document("transactions/$trxID")
        val packageDocRef = db.collection("/users/$userContact/packages").document(HelperClass.getRandomString(20))
        val dataMap = mutableMapOf<String,Any>()
        dataMap["name"] = "20 Exams For Three Days"
        dataMap["remainingExams"] = 20
        dataMap["expiredAt"] = getTimestampAfter(3)
        db.runTransaction { transaction ->
            transaction.update(transactionDocRef,"status","approved")
            transaction.set(packageDocRef,dataMap)
            null
        }.addOnSuccessListener {
            makeToast("Success!")
        }.addOnFailureListener { e ->
            Timber.e(e)
            makeToast(e.message)
        }
    }

    fun sevenDays20xm() {
        val transactionDocRef = db.document("transactions/$trxID")
        val packageDocRef = db.collection("/users/$userContact/packages").document(HelperClass.getRandomString(20))
        val dataMap = mutableMapOf<String,Any>()
        dataMap["name"] = "20 Exams For Seven Days"
        dataMap["remainingExams"] = 20
        dataMap["expiredAt"] = getTimestampAfter(7)
        db.runTransaction { transaction ->
            transaction.update(transactionDocRef,"status","approved")
            transaction.set(packageDocRef,dataMap)
            null
        }.addOnSuccessListener {
            makeToast("Success!")
        }.addOnFailureListener { e ->
            Timber.e(e)
            makeToast(e.message)
        }
    }

    fun oneMonth20xm() {
        val transactionDocRef = db.document("transactions/$trxID")
        val packageDocRef = db.collection("/users/$userContact/packages").document(HelperClass.getRandomString(20))
        val dataMap = mutableMapOf<String,Any>()
        dataMap["name"] = "20 Exams For 1 Month"
        dataMap["remainingExams"] = 20
        dataMap["expiredAt"] = getTimestampAfter(30)
        db.runTransaction { transaction ->
            transaction.update(transactionDocRef,"status","approved")
            transaction.set(packageDocRef,dataMap)
            null
        }.addOnSuccessListener {
            makeToast("Success!")
        }.addOnFailureListener { e ->
            Timber.e(e)
            makeToast(e.message)
        }
    }

    fun oneMonth100xm() {
        val transactionDocRef = db.document("transactions/$trxID")
        val packageDocRef = db.collection("/users/$userContact/packages").document(HelperClass.getRandomString(20))
        val dataMap = mutableMapOf<String,Any>()
        dataMap["name"] = "100 Exams For 1 Month"
        dataMap["remainingExams"] = 100
        dataMap["expiredAt"] = getTimestampAfter(30)
        db.runTransaction { transaction ->
            transaction.update(transactionDocRef,"status","approved")
            transaction.set(packageDocRef,dataMap)
            null
        }.addOnSuccessListener {
            makeToast("Success!")
        }.addOnFailureListener { e ->
            Timber.e(e)
            makeToast(e.message)
        }
    }

    fun unlimitedOneMonth() {
        val transactionDocRef = db.document("transactions/$trxID")
        val packageDocRef = db.collection("/users/$userContact/packages").document(HelperClass.getRandomString(20))
        val dataMap = mutableMapOf<String,Any>()
        dataMap["name"] = "Unlimited Exams For One Month"
        dataMap["remainingExams"] = 10000000
        dataMap["expiredAt"] = getTimestampAfter(30)
        db.runTransaction { transaction ->
            transaction.update(transactionDocRef,"status","approved")
            transaction.set(packageDocRef,dataMap)
            null
        }.addOnSuccessListener {
            makeToast("Success!")
        }.addOnFailureListener { e ->
            Timber.e(e)
            makeToast(e.message)
        }
    }

    fun unlimitedThreeMonth() {
        val transactionDocRef = db.document("transactions/$trxID")
        val packageDocRef = db.collection("/users/$userContact/packages").document(HelperClass.getRandomString(20))
        val dataMap = mutableMapOf<String,Any>()
        dataMap["name"] = "Unlimited Exams For Three Month"
        dataMap["remainingExams"] = 10000000
        dataMap["expiredAt"] = getTimestampAfter(92)
        db.runTransaction { transaction ->
            transaction.update(transactionDocRef,"status","approved")
            transaction.set(packageDocRef,dataMap)
            null
        }.addOnSuccessListener {
            makeToast("Success!")
        }.addOnFailureListener { e ->
            Timber.e(e)
            makeToast(e.message)
        }
    }

    private fun getTimestampAfter(days: Int): Timestamp {
        val currentDate = Calendar.getInstance()
        currentDate.add(Calendar.DATE,days)
        return Timestamp(currentDate.time)
    }

    private fun initializeProperties() {
        trxID = arguments?.getString("trxID")
        userContact = arguments?.getString("phoneNo")
        binding.presenter = this
    }
}
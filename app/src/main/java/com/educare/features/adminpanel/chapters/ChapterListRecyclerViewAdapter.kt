package com.educare.features.adminpanel.chapters

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.Chapter

class ChapterListRecyclerViewAdapter: SingleItemBaseAdapter<Chapter>() {

    private val chapters = mutableListOf<Chapter>()

    var onItemClicked: ((Chapter) -> Unit)? = null
    var onLongClicked: ((Chapter) -> Unit)? = null

    fun updateItems(updatedChapters: List<Chapter>) {
        chapters.clear()
        chapters.addAll(updatedChapters)
        notifyDataSetChanged()
    }

    override fun clickedItem(item: Chapter) {
        onItemClicked?.invoke(item)
    }

    override fun getItemForPosition(position: Int): Chapter {
        return chapters[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_chapters
    }

    override fun getItemCount(): Int {
        return chapters.size
    }

    override fun onLongClicked(item: Chapter): Boolean {
        onLongClicked?.invoke(item)
        return true
    }
}
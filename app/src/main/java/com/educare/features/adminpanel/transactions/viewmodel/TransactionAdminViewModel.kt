package com.educare.features.adminpanel.transactions.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.educare.common.models.TransactionDetails
import com.educare.common.repos.GlobalRepository
import kotlinx.coroutines.flow.Flow

class TransactionAdminViewModel: ViewModel() {

    private val repo by lazy { GlobalRepository() }

    val toast: MutableLiveData<String> = MutableLiveData()

    var lastPath: String? = null

    var transactionDetails: Flow<PagingData<TransactionDetails>>? = null

    fun loadTransactionDetailsStream(path: String): Flow<PagingData<TransactionDetails>>? {
        val lastResult = transactionDetails
        return if (path == lastPath && lastResult!=null) {
            lastResult
        } else {
            lastPath = path
            val newResult = repo.fetchTransactionDetailsStream(path).cachedIn(viewModelScope)
            transactionDetails = newResult
            newResult
        }
    }

}
package com.educare.features.adminpanel.mcqpractice.ui

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.toVisibility
import com.educare.common.loadstatefooter.FooterLoadStateAdapter
import com.educare.common.models.DataBindingFragment
import com.educare.common.models.MCQQuestion
import com.educare.databinding.FragmentMCQPracticeAdminBinding
import com.educare.features.adminpanel.mcqstudy.recycleradapter.MCQListRecyclerAdapter
import com.educare.features.adminpanel.mcqstudy.viewmodel.MCQStudyFragmentAdminViewModel
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MCQPracticeAdmin : DataBindingFragment<FragmentMCQPracticeAdminBinding>(R.layout.fragment_m_c_q_practice_admin) {

    private var collectionPath: String? = null

    private val adapter by lazy { MCQListRecyclerAdapter() }

    private var questionFetchJob: Job? = null

    private val viewModel: MCQStudyFragmentAdminViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchQuestions()
        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launch {
                adapter.refresh()
                delay(1500)
                binding.swipeRefresh.isRefreshing = false
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeObservers()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun fetchQuestions() {
        questionFetchJob?.cancel()
        questionFetchJob = lifecycleScope.launch {
            viewModel.fetchQuestions(collectionPath!!)?.collectLatest {
                adapter.submitData(it)
            }
        }
    }

    fun addQuestion() {
        findNavController().navigate(R.id.action_MCQPracticeAdmin_to_addMCQ, bundleOf("path" to collectionPath))
    }

    fun reload() {
        adapter.retry()
    }

    private fun initializeProperties() {
        binding.presenter = this
        collectionPath = arguments?.getString("path")
    }

    private fun showOptionsDialogMenu(question: MCQQuestion) {
        AlertDialog.Builder(context).apply {
            setItems(arrayOf("Edit","Delete")) { _, position ->
                if(position==0) {
                    findNavController().navigate(
                        R.id.action_MCQPracticeAdmin_to_editMCQAdmin,
                        bundleOf("question" to convertQuestionToJSON(question),"path" to collectionPath)
                    )
                } else if (position == 1) {
                    deleteQuestionFromDB(question.docID)
                }
            }
        }.create().show()
    }

    private fun deleteQuestionFromDB(docID: String) {
        viewModel.deleteQuestion(collectionPath!!,docID)
    }

    private fun convertQuestionToJSON(question: MCQQuestion): String? {
        val adapter = Moshi.Builder().build().adapter(MCQQuestion::class.java)
        return adapter.toJson(question)
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner, Observer { makeToast(it) })
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
            header = FooterLoadStateAdapter { adapter.retry() },
            footer = FooterLoadStateAdapter { adapter.retry() }
        )

        adapter.onLongClick = {
            showOptionsDialogMenu(it)
        }

        adapter.addLoadStateListener { loadState ->
            if (loadState.refresh !is LoadState.NotLoading) {
                // If we are refreshing data from server then we have to hid
                // the list and show progressbar or in case of an error we have
                // to show the retry button
                binding.recyclerView.visibility = View.GONE
                binding.loadingContent.visibility = toVisibility(loadState.refresh is LoadState.Loading)
                binding.retry.visibility = toVisibility(loadState.refresh is LoadState.Error)
            } else {
                // If not loading data then show the list
                binding.recyclerView.visibility = View.VISIBLE
                binding.loadingContent.visibility = View.GONE
                binding.retry.visibility = View.GONE

                val errorState = when {
                    loadState.append is LoadState.Error -> {
                        loadState.append as LoadState.Error
                    }
                    loadState.prepend is LoadState.Error -> {
                        loadState.prepend as LoadState.Error
                    }
                    else -> null
                }
                errorState?.let { makeToast("${it.error.message}") }
            }
        }
    }

}
package com.educare.features.adminpanel.availablequestions

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.educare.R
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentAvailableQuestionsBinding

class AvailableQuestionsFragment : DataBindingFragment<FragmentAvailableQuestionsBinding>(R.layout.fragment_available_questions) {

    private val navController by lazy { findNavController() }

    private var collectionPath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this

        collectionPath = arguments?.getString("path")
        initializeTitles()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    fun navigateToMcqStudy() {
        navController.navigate(R.id.action_availableQuestionsFragment_to_MCQStudyFragmentAdmin, bundleOf("path" to "$collectionPath/mcq-study"))
    }

/*    fun navigateToMcqPractice() {
        navController.navigate(R.id.action_availableQuestionsFragment_to_MCQPracticeAdmin, bundleOf("path" to "$collectionPath/mcq-practice"))
    }*/

    fun navigateToMcqQuiz() {
        navController.navigate(R.id.action_availableQuestionsFragment_to_MCQQuizAdmin, bundleOf("path" to "$collectionPath/quiz-questions"))
    }

    fun navigateToLectureVideos() {
        navController.navigate(R.id.action_availableQuestionsFragment_to_lectureVideosAdmin,bundleOf("path" to "$collectionPath/lecture-videos"))
    }

    fun navigateToLectureSheets() {
        navController.navigate(R.id.action_availableQuestionsFragment_to_lectureSheetsAdmin,bundleOf("path" to "$collectionPath/lecture-sheets"))
    }

    private fun initializeTitles() {
        binding.mcqStudy.title.text = "MCQ Study"
        //binding.mcqPractice.title.text = "MCQ Practice"
        binding.mcqQuiz.title.text = "MCQ Quizes"
        binding.lectureVideos.title.text = "Video Lectures"
        binding.lectureSheets.title.text = "Lecture Sheets"
    }

}
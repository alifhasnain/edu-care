package com.educare.features.adminpanel.lecturesheets

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.LectureSheet

class LectureSheetsRecyclerAdapter: SingleItemBaseAdapter<LectureSheet>() {

    private val lectureSheets = mutableListOf<LectureSheet>()

    var onItemClick: ((LectureSheet) -> Unit)? = null
    var onLongClick: ((LectureSheet) -> Unit)? = null

    fun updateItems(updatedItems: List<LectureSheet>) {
        lectureSheets.clear()
        lectureSheets.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: LectureSheet): Boolean {
        onLongClick?.invoke(item)
        return true
    }

    override fun clickedItem(item: LectureSheet) {
        onItemClick?.invoke(item)
    }

    override fun getItemForPosition(position: Int): LectureSheet {
        return lectureSheets[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_lecture_sheets
    }

    override fun getItemCount(): Int {
        return lectureSheets.size
    }
}
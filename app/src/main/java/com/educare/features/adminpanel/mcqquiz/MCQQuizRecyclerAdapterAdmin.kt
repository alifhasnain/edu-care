package com.educare.features.adminpanel.mcqquiz

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.educare.R
import com.educare.common.diffcallbacks.MCQQuestionDiffCallback
import com.educare.common.models.MCQQuestion
import com.educare.common.models.MCQQuestionList
import com.educare.databinding.ListItemMcqQuizAdminBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MCQQuizRecyclerAdapterAdmin: RecyclerView.Adapter<MCQQuizRecyclerAdapterAdmin.ViewHolder>() {

    val questions = mutableListOf<MCQQuestion>()

    var onLongClick: ((MCQQuestion) -> Unit)? = null

    fun updateItems(updatedQuestions: MCQQuestionList) {
        CoroutineScope(Dispatchers.Main).launch {
            val diffResult = withContext(Dispatchers.Default) {
                val result = DiffUtil.calculateDiff(MCQQuestionDiffCallback(questions,updatedQuestions.questions))
                questions.clear()
                questions.addAll(updatedQuestions.questions)
                result
            }
            diffResult.dispatchUpdatesTo(this@MCQQuizRecyclerAdapterAdmin)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ListItemMcqQuizAdminBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_item_mcq_quiz_admin,
            parent,
            false
        )
        val viewHolder = ViewHolder(binding)
        binding.rootLayout.setOnLongClickListener {
            onLongClick?.invoke(questions[viewHolder.absoluteAdapterPosition])
            true
        }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return questions.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(questions[position])
    }

    class ViewHolder(private val binding: ListItemMcqQuizAdminBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MCQQuestion) {
            binding.checkbox1.apply {
                isChecked = false
                isClickable = false
            }
            binding.checkbox2.apply {
                isChecked = false
                isClickable = false
            }
            binding.checkbox3.apply {
                isChecked = false
                isClickable = false
            }
            binding.checkbox4.apply {
                isChecked = false
                isClickable = false
            }

            when(item.ans) {
                0 -> binding.checkbox1.isChecked = true
                1 -> binding.checkbox2.isChecked = true
                2 -> binding.checkbox3.isChecked = true
                3 -> binding.checkbox4.isChecked = true
            }

            binding.question.setDisplayText("${absoluteAdapterPosition+1}. ${item.question}")
            binding.choiceOne.setDisplayText(item.choices[0])
            binding.choiceTwo.setDisplayText(item.choices[1])
            binding.choiceThree.setDisplayText(item.choices[2])
            binding.choiceFour.setDisplayText(item.choices[3])
        }
    }

}
package com.educare.features.adminpanel.lecturesheets

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.dp
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.runWithoutException
import com.educare.common.extensions.trimmedString
import com.educare.common.models.LectureSheet
import com.educare.common.repos.GlobalRepository
import com.educare.databinding.FragmentLectureSheetsAdminBinding
import com.educare.features.pdfview.PDFViewActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.wada811.viewbinding.viewBinding
import kotlinx.coroutines.launch
import timber.log.Timber


class LectureSheetsAdmin : Fragment(R.layout.fragment_lecture_sheets_admin) {

    private val binding: FragmentLectureSheetsAdminBinding by viewBinding { FragmentLectureSheetsAdminBinding.bind(it) }

    private val adapter by lazy { LectureSheetsRecyclerAdapter() }

    private val repo by lazy { GlobalRepository() }

    private var collectionPath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchLectureSheets()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("path",collectionPath)
        super.onSaveInstanceState(outState)
    }

    private fun addNewSheet() {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL

        val title = EditText(context)
        title.hint = "Title"
        layout.addView(title)

        val docURL = EditText(context)
        docURL.hint = "Document URL"
        layout.addView(docURL)

        (title.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,12.dp,8.dp,4.dp)
        }
        (docURL.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,4.dp,8.dp,12.dp)
        }

        AlertDialog.Builder(requireContext()).apply {
            setTitle("Type the chapter name")
            setView(layout)
            setPositiveButton("OK") { _,_ ->
                addLectureSheetToDB(LectureSheet(title.trimmedString,docURL.trimmedString))
                makeToast("Please wait while adding data")
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun addLectureSheetToDB(lectureSheet: LectureSheet) {
        if(lectureSheet.title.isEmpty() || lectureSheet.url.isEmpty()) {
            makeToast("Empty text is not allowed")
        } else {
            safeScope {
                makeToast("Adding data please wait")
                repo.addLectureSheet("$collectionPath", lectureSheet)
                makeToast("Added successfully")
            }
        }
    }

    private fun showContextMenu(lectureSheet: LectureSheet) {
        AlertDialog.Builder(context).apply {
            setItems(arrayOf("Edit","Delete")) { _, position ->
                if(position==0) {
                    editLectureSheet(lectureSheet)
                } else if (position == 1) {
                    deleteLectureSheet(lectureSheet.docID)
                }
            }
        }.create().show()
    }

    private fun editLectureSheet(lectureSheet: LectureSheet) {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL

        val title = EditText(context)
        title.hint = "Title"
        title.setText(lectureSheet.title)
        layout.addView(title)

        val docURL = EditText(context)
        docURL.hint = "Document URL"
        docURL.setText(lectureSheet.url)
        layout.addView(docURL)

        (title.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,12.dp,8.dp,4.dp)
        }
        (docURL.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,4.dp,8.dp,12.dp)
        }

        AlertDialog.Builder(requireContext()).apply {
            setTitle("Type the chapter name")
            setView(layout)
            setPositiveButton("OK") { _,_ ->
                updateLectureSheet(LectureSheet(title.trimmedString,docURL.trimmedString,lectureSheet.docID))
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun updateLectureSheet(lectureSheet: LectureSheet) {
        safeScope {
            repo.updateLectureSheet(collectionPath!!,lectureSheet)
            makeToast("Updated")
        }
    }

    private fun deleteLectureSheet(docID: String) {
        safeScope {
            makeToast("deleting")
            repo.deleteLectureSheet(collectionPath!!,docID)
            makeToast("deleted successfully")
        }
    }

    private fun fetchLectureSheets() {
        FirebaseFirestore.getInstance().collection(collectionPath!!).addSnapshotListener(requireActivity()) { querySnapshot, e ->
            runWithoutException {
                if (e != null) {
                    makeToast(e.message)
                } else {
                    adapter.updateItems(querySnapshot!!.map { it.toObject<LectureSheet>().apply { docID = it.id } })
                }
            }
        }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onItemClick = {
            startActivity(Intent(activity,PDFViewActivity::class.java).apply {
                putExtras(bundleOf("url" to it.url))
            })
        }
        adapter.onLongClick = {
            showContextMenu(it)
        }
    }

    private fun initializeProperties() {
        collectionPath = arguments?.getString("path")
        binding.addSheet.setOnClickListener { addNewSheet() }
    }

    private fun safeScope(block: (suspend () -> Unit)) {
        lifecycleScope.launch {
            try {
                block()
            } catch (e: Exception) {
                makeToast(e.localizedMessage)
                Timber.e(e)
            }
        }
    }

}
package com.educare.features.adminpanel.home

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.educare.R
import com.educare.common.models.DataBindingFragment
import com.educare.databinding.FragmentAdminPanelHomeBinding


class AdminPanelHomeFragment : DataBindingFragment<FragmentAdminPanelHomeBinding>(R.layout.fragment_admin_panel_home) {

    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this

        initializeTitles()

    }

    fun navigateToClasses() {
        navController.navigate(R.id.action_adminPanelHomeFragment_to_availableClasses)
    }

    fun navigateToCreateCoupon() {
        navController.navigate(R.id.action_adminPanelHomeFragment_to_createCoupon)
    }

    fun navigateToCouponList() {
        navController.navigate(R.id.action_adminPanelHomeFragment_to_couponListAdmin)
    }

    fun navigateToTransactionList() {
        navController.navigate(R.id.action_adminPanelHomeFragment_to_transactionAdmin)
    }

    private fun initializeTitles() {
        binding.allClasses.title.text = "All Classes"
        binding.createCoupon.title.text = "Create Coupon"
        binding.couponList.title.text = "Coupon List"
        binding.transactionList.title.text = "Transaction List"
    }
}
package com.educare.features.transactions

import com.educare.R
import com.educare.common.adapter.SingleItemBaseAdapter
import com.educare.common.models.TransactionDetails

class TransactionStudentRecyclerAdapter: SingleItemBaseAdapter<TransactionDetails>() {

    private val items = mutableListOf<TransactionDetails>()

    fun updateItems(updatedItems: List<TransactionDetails>) {
        items.clear()
        items.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onLongClicked(item: TransactionDetails): Boolean {
        return true
    }

    override fun clickedItem(item: TransactionDetails) {

    }

    override fun getItemForPosition(position: Int): TransactionDetails {
        return items[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_transaction_student
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
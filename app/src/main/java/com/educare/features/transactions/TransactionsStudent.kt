package com.educare.features.transactions

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.educare.R
import com.educare.common.extensions.dp
import com.educare.common.extensions.makeToast
import com.educare.common.extensions.trimmedString
import com.educare.common.models.TransactionDetails
import com.educare.databinding.FragmentTransactionsStudentBinding
import com.educare.utils.ProfileSharedPrefHelper
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.wada811.viewbinding.viewBinding
import org.json.JSONObject
import timber.log.Timber


class TransactionsStudent : Fragment(R.layout.fragment_transactions_student) {

    private val binding by viewBinding { FragmentTransactionsStudentBinding.bind(it) }

    private val adapter = TransactionStudentRecyclerAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeRecyclerView()
        fetchTransactions()
    }

    private fun fetchTransactions() {
        FirebaseFirestore.getInstance().collection("transactions")
            .limit(15)
            .orderBy("requestTime")
            .whereEqualTo("userContact",ProfileSharedPrefHelper.getUserProfile(requireContext())?.phoneNo)
            .addSnapshotListener { querySnapshot, e ->
                if (e != null) {
                    makeToast("Error occurred while getting data. Please check your connection.")
                    Timber.e(e)
                } else {
                    adapter.updateItems(querySnapshot!!.toObjects(TransactionDetails::class.java))
                }
            }
    }

    private fun addTransactionInfo() {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL

        val transactionID = EditText(context)
        transactionID.hint = "Transaction ID (TrxID)"
        layout.addView(transactionID)

        val contactNo = EditText(context)
        contactNo.inputType = InputType.TYPE_CLASS_NUMBER
        contactNo.hint = "Contact No From Which Transaction Was Done"
        layout.addView(contactNo)

        (transactionID.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,12.dp,8.dp,4.dp)
        }
        (contactNo.layoutParams as ViewGroup.MarginLayoutParams).apply {
            updateMargins(8.dp,4.dp,8.dp,12.dp)
        }

        AlertDialog.Builder(requireContext()).apply {
            setTitle("Please type the transaction id and the contact no from which transaction was done")
            setView(layout)
            setPositiveButton("OK") { _,_ ->
                requestTransactionReview(transactionID.trimmedString,contactNo.trimmedString)
            }
            setNegativeButton("Cancel",null)
        }.create().show()
    }

    private fun requestTransactionReview(trxID: String, contactNo: String) {
        val jsonObject = JSONObject()
        jsonObject.put("trxID",trxID)
        jsonObject.put("contactNo",contactNo)

        FirebaseFunctions.getInstance("asia-east2")
            .getHttpsCallable("requestTransactionVerification")
            .call(jsonObject.toString())
            .addOnSuccessListener {
                makeToast(it.data.toString())
            }
            .addOnFailureListener {
                Timber.e(it)
                makeToast(it.message)
            }
    }

    private fun initializeProperties() {
        binding.addTransaction.setOnClickListener { addTransactionInfo() }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

}